package com.geeklabs.attendancesystem.repository;

import java.util.List;

import com.geeklabs.attendancesystem.domain.Student;
import com.geeklabs.attendancesystem.repository.objectify.ObjectifyCRUDRepository;

public interface StudentRepository extends ObjectifyCRUDRepository<Student>{
	List<Student> getStudentsByBranchAndYear(String branch, String year);
	Student getStudentByEmail(String email);
}
