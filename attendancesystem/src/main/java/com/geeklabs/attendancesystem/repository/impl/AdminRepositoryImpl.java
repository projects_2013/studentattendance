package com.geeklabs.attendancesystem.repository.impl;

import org.springframework.beans.factory.annotation.Autowired;

import com.geeklabs.attendancesystem.domain.Admin;
import com.geeklabs.attendancesystem.repository.AdminRepository;
import com.geeklabs.attendancesystem.repository.objectify.AbstractObjectifyCRUDRepository;
import com.googlecode.objectify.Objectify;

public class AdminRepositoryImpl extends AbstractObjectifyCRUDRepository<Admin> implements AdminRepository{

	@Autowired
	private Objectify objectify;
	
	public AdminRepositoryImpl() {
		super(Admin.class);
	}
	
	@Override
	protected Objectify getObjectify() {
		return objectify;
	}
	
	public Admin getAdminByEmail(String email) {
		return objectify.load()
				.type(Admin.class)
				.filter("email", email)
				.first()
				.now();
	}
}
