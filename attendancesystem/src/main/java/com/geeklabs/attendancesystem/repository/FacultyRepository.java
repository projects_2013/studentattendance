package com.geeklabs.attendancesystem.repository;

import com.geeklabs.attendancesystem.domain.Faculty;
import com.geeklabs.attendancesystem.repository.objectify.ObjectifyCRUDRepository;

public interface FacultyRepository extends ObjectifyCRUDRepository<Faculty>{
	Faculty getFacultyByEmail(String email);
}
