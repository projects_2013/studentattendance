package com.geeklabs.attendancesystem.repository.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.geeklabs.attendancesystem.domain.Student;
import com.geeklabs.attendancesystem.repository.StudentRepository;
import com.geeklabs.attendancesystem.repository.objectify.AbstractObjectifyCRUDRepository;
import com.googlecode.objectify.Objectify;

public class StudentRepositoryImpl extends AbstractObjectifyCRUDRepository<Student> implements StudentRepository {

	@Autowired
	private Objectify objectify;
	
	public StudentRepositoryImpl() {
		super(Student.class);
	}
	
	@Override
	protected Objectify getObjectify() {
		return objectify;
	}

	@Override
	public List<Student> getStudentsByBranchAndYear(String branch, String year) {
		return objectify.load()
				  .type(Student.class)
				  .filter("branch", branch)
				  .filter("year", year)
				  .list();
	}

	@Override
	public Student getStudentByEmail(String email) {
		return objectify.load()
				.type(Student.class)
				.filter("studentEmail", email)
				.first()
				.now();
	}

}
