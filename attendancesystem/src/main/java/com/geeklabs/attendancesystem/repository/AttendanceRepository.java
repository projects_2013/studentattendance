package com.geeklabs.attendancesystem.repository;

import java.util.List;

import com.geeklabs.attendancesystem.domain.Attendance;
import com.geeklabs.attendancesystem.repository.objectify.ObjectifyCRUDRepository;

public interface AttendanceRepository extends ObjectifyCRUDRepository<Attendance>{

	List<Attendance> getAttendanceByBranchAndYear(String branch, String year);
	List<Attendance> getAttendanceForSpecificStudent(String studentId);
	boolean isAttendanceExist(Long studentId, String date, String time, boolean attendanceStatus);
}
