package com.geeklabs.attendancesystem.repository.impl;

import org.springframework.beans.factory.annotation.Autowired;

import com.geeklabs.attendancesystem.domain.Faculty;
import com.geeklabs.attendancesystem.repository.FacultyRepository;
import com.geeklabs.attendancesystem.repository.objectify.AbstractObjectifyCRUDRepository;
import com.googlecode.objectify.Objectify;

public class FacultyRepositoryImpl extends AbstractObjectifyCRUDRepository<Faculty> implements FacultyRepository {

	@Autowired
	private Objectify objectify;
	
	public FacultyRepositoryImpl() {
		super(Faculty.class);
	}
	
	@Override
	protected Objectify getObjectify() {
		return objectify;
	}

	@Override
	public Faculty getFacultyByEmail(String email) {
		return objectify.load()
						.type(Faculty.class)
						.filter("email", email)
						.first()
						.now();
	}
}
