package com.geeklabs.attendancesystem.repository;

import com.geeklabs.attendancesystem.domain.Admin;
import com.geeklabs.attendancesystem.repository.objectify.ObjectifyCRUDRepository;

public interface AdminRepository extends ObjectifyCRUDRepository<Admin>{
	 Admin getAdminByEmail(String email);
}
