package com.geeklabs.attendancesystem.repository.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.geeklabs.attendancesystem.domain.Attendance;
import com.geeklabs.attendancesystem.domain.Student;
import com.geeklabs.attendancesystem.repository.AttendanceRepository;
import com.geeklabs.attendancesystem.repository.objectify.AbstractObjectifyCRUDRepository;
import com.googlecode.objectify.Objectify;

public class AttendanceRepositoryImpl extends AbstractObjectifyCRUDRepository<Attendance> implements AttendanceRepository {
	
	@Autowired
	private Objectify objectify;
	
	@Override
	protected Objectify getObjectify() {
		return objectify;
	}
	
	public AttendanceRepositoryImpl() {
		super(Attendance.class);
	}

	@Override
	public List<Attendance> getAttendanceByBranchAndYear(String branch, String year) {
		return objectify.load()
				 .type(Attendance.class)
				 .filter("branch", branch)
				 .filter("year", year)
				 .list();
	}

	@Override
	public List<Attendance> getAttendanceForSpecificStudent(String studentId) {
		Long id = Long.valueOf(studentId);
		 return objectify.load()
				 .type(Attendance.class)
				 .filter("student",objectify.load().type(Student.class).id(id).now())
				 .list();
	}

	@Override
	public boolean isAttendanceExist(Long studentId, String date, String time, boolean attendanceStatus) {
		Long id = Long.valueOf(studentId);
		 List<Attendance> list = objectify.load()
				 						  .type(Attendance.class)
										  .filter("student", objectify.load().type(Student.class).id(id).now())
										  .filter("date", date)
										  .filter("time", time)
										  .filter("attendanceStatus", attendanceStatus)
									      .list();
		 if (list != null && !list.isEmpty()) {
			return true;
		}
		return false;
	}
}
