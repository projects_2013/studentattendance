package com.geeklabs.attendancesystem.util;

import java.util.TimerTask;

import com.geeklabs.attendancesystem.domain.Attendance;
import com.geeklabs.attendancesystem.domain.Student;
import com.geeklabs.attendancesystem.service.EmailService;
import com.geeklabs.attendancesystem.service.impl.EmailServiceImpl;

public class TimerSchedular extends TimerTask {
	private Student student;
	private Attendance attendance;

	public TimerSchedular(Student student, Attendance attendance) {
		this.student = student;
		this.attendance = attendance;
	}

	@Override
	public void run() {
		EmailService emailServiceImpl = new EmailServiceImpl();
		emailServiceImpl.send(student.getStudentEmail(), "about attendance", "Attendance status /n/n"+attendance.getAttendanceStatus()+"on"+attendance.getDate()+"and"+ attendance.getTime());
		emailServiceImpl.send(student.getParentEmail(), "about attendance", "Attendance status about your child /n/n"+attendance.getAttendanceStatus()+"on"+attendance.getDate()+"and"+ attendance.getTime());
	}
}
