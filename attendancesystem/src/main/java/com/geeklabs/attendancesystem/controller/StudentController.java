package com.geeklabs.attendancesystem.controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.geeklabs.attendancesystem.domain.Student;
import com.geeklabs.attendancesystem.dto.StudentDto;
import com.geeklabs.attendancesystem.service.StudentService;
import com.geeklabs.attendancesystem.util.ResponseStatus;

@RequestMapping(value = "student")
@Controller
public class StudentController {

	private StudentService studentService;

	@RequestMapping(value = "save", method = RequestMethod.POST)
	public @ResponseBody ResponseStatus saveStudentDetails(@RequestBody Student student) {
		return studentService.saveStudent(student);
	}
	@RequestMapping(value = "getStudents/{branch}/{year}", method = RequestMethod.GET)
	public @ResponseBody List<StudentDto> getStudentsDetails(@PathVariable String branch, @PathVariable String year){
		return studentService.getStudents(branch, year);
	}

	public void setStudentService(StudentService studentService) {
		this.studentService = studentService;
	}
}
