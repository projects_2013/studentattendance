package com.geeklabs.attendancesystem.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.geeklabs.attendancesystem.domain.Admin;
import com.geeklabs.attendancesystem.service.AdminService;
import com.geeklabs.attendancesystem.util.ResponseStatus;


@RequestMapping(value = "admin")
public class AdminController {
	private AdminService adminService;
	
	public void setAdminService(AdminService adminService) {
		this.adminService = adminService;
	}
	
	@RequestMapping(value = "save", method = RequestMethod.GET)
	public @ResponseBody ResponseStatus saveAdmin() {
		Admin admin = new Admin();
		admin.setEmail("andheranjeetha@gmail.com");
		admin.setName("Ranjeetha");
		return adminService.saveAdmin(admin);
	}
}
