package com.geeklabs.attendancesystem.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.geeklabs.attendancesystem.dto.LoginAs;
import com.geeklabs.attendancesystem.service.AuthenticationService;
import com.geeklabs.attendancesystem.util.ResponseStatus;

@RequestMapping(value = "auth")
public class AuthenticationController {
	private AuthenticationService authenticationService;
	
	public void setAuthenticationService(
			AuthenticationService authenticationService) {
		this.authenticationService = authenticationService;
	}
	
	@RequestMapping(value = "authToken", method = RequestMethod.POST)
	public @ResponseBody ResponseStatus authenticateEmail(@RequestBody LoginAs loginAs) {
		return authenticationService.authenticateGivenAuthToken(loginAs);
	}
	
	@RequestMapping(value = "signOut/{id}/{loginType}", method = RequestMethod.GET)
	public @ResponseBody ResponseStatus signOut(@PathVariable String id, @PathVariable String loginType) {
		return authenticationService.signOut(id, loginType);
	}
}
