package com.geeklabs.attendancesystem.controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.geeklabs.attendancesystem.domain.Faculty;
import com.geeklabs.attendancesystem.dto.FacultyDto;
import com.geeklabs.attendancesystem.service.FacultyService;
import com.geeklabs.attendancesystem.util.ResponseStatus;

@RequestMapping(value = "faculty")
@Controller
public class FacultyController {

	private FacultyService facultyService;

	@RequestMapping(value = "save", method = RequestMethod.POST)
	@ResponseBody
	public ResponseStatus saveFacaltyDetails(@RequestBody Faculty faculty) {
		return facultyService.saveFaculty(faculty);
	}
	
	@RequestMapping(value = "getFaculty", method = RequestMethod.GET)
	public @ResponseBody List<FacultyDto> getFacultyDetails() {
		return facultyService.getFaculties();
	}

	public void setFacultyService(FacultyService facultyService) {
		this.facultyService = facultyService;
	}
}
