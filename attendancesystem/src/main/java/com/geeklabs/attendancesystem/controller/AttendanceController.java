package com.geeklabs.attendancesystem.controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.geeklabs.attendancesystem.dto.AttendanceDto;
import com.geeklabs.attendancesystem.service.AttendanceService;
import com.geeklabs.attendancesystem.util.ResponseStatus;

@RequestMapping(value = "attendance")
@Controller
public class AttendanceController {

	private AttendanceService attendanceService;
	
	public void setAttendanceService(AttendanceService attendanceService) {
		this.attendanceService = attendanceService;
	}
	
	@RequestMapping(value = "save", method = RequestMethod.POST)
	@ResponseBody
	public ResponseStatus saveAttendanceDeatails(@RequestBody List<AttendanceDto> attendanceDtos){
		attendanceService.saveAttendance(attendanceDtos);
		ResponseStatus responseStatus = new ResponseStatus();
		responseStatus.setStatus("success");
		return responseStatus;
	}
	
	@RequestMapping(value = "sample", method = RequestMethod.POST)
	@ResponseBody
	void saveData(){
		attendanceService.sampleTest();
	}
	
	@RequestMapping(value = "getAttendance/{branch}/{year}", method = RequestMethod.GET)
	public @ResponseBody List<AttendanceDto> getAttendance(@PathVariable String branch, @PathVariable String year){
		return attendanceService.getAttendanceByBranchAndYear(branch, year);
	}
	
	@RequestMapping(value = "getSingleStudentAttendance/{studentId}", method = RequestMethod.GET)
	public @ResponseBody List<AttendanceDto> getAttendanceForSpecificStudent(@PathVariable String studentId){
		return attendanceService.getAttendanceForSpecificStudent(studentId);
	}
}
