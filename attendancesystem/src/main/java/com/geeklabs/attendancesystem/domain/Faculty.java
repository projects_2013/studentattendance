package com.geeklabs.attendancesystem.domain;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

@Entity
public class Faculty {
	
	public static Key<Faculty> key(long id) {
		  return Key.create(Faculty.class, id);
		}
		
	@Id
	private Long id;
	private String name;
	@Index
	private String email;
	private boolean signInStatus = false;
	
	public Long getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public boolean isSignInStatus() {
		return signInStatus;
	}
	public void setSignInStatus(boolean signInStatus) {
		this.signInStatus = signInStatus;
	}
}
