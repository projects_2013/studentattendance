package com.geeklabs.attendancesystem.domain;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

@Entity
public class Admin {
	
	@Id
	private Long id;
	@Index
	private String email;
	private String name;
	private boolean signInStatus = false;
	
	public Long getId() {
		return id;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public boolean isSignInStatus() {
		return signInStatus;
	}
	
	public void setSignInStatus(boolean signInStatus) {
		this.signInStatus = signInStatus;
	}
}
