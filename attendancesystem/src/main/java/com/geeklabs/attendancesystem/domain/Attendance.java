package com.geeklabs.attendancesystem.domain;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

@Entity
public class Attendance {

	@Id
	private Long id;
	@Index
	private Key<Faculty> faculty;
	@Index
	private Key<Student> student;
	@Index
	private String date;
	@Index
	private String time;
	@Index
	private String year;
	@Index
	private String branch;
	@Index
	private boolean attendanceStatus;
	
	public Long getId() {
		return id;
	}
	
	public Key<Faculty> getFaculty() {
		return faculty;
	}
	
	public void setFaculty(Key<Faculty> faculty) {
		this.faculty = faculty;
	}
	
	public Key<Student> getStudent() {
		return student;
	}
	
	public void setStudent(Key<Student> student) {
		this.student = student;
	}
	
	public String getDate() {
		return date;
	}
	
	public void setDate(String date) {
		this.date = date;
	}
	
	public String getTime() {
		return time;
	}
	
	public void setTime(String time) {
		this.time = time;
	}
	
	public String getYear() {
		return year;
	}
	
	public void setYear(String year) {
		this.year = year;
	}
	
	public String getBranch() {
		return branch;
	}
	
	public void setBranch(String branch) {
		this.branch = branch;
	}
	
	public boolean getAttendanceStatus() {
		return attendanceStatus;
	}
	public void setAttendanceStatus(boolean attendanceStatus) {
		this.attendanceStatus = attendanceStatus;
	}
}
