package com.geeklabs.attendancesystem.facade;

import com.geeklabs.attendancesystem.domain.Attendance;
import com.geeklabs.attendancesystem.dto.AttendanceDto;

public interface AttendanceFacade {
	Attendance convertAttendanceDtoToAttendanceDomain(AttendanceDto attendanceDto);
	AttendanceDto convertAttedanceDomainToAttendanceDto(Attendance attendance);

}
