package com.geeklabs.attendancesystem.facade.impl;

import org.dozer.DozerBeanMapper;

import com.geeklabs.attendancesystem.domain.Attendance;
import com.geeklabs.attendancesystem.domain.Faculty;
import com.geeklabs.attendancesystem.domain.Student;
import com.geeklabs.attendancesystem.dto.AttendanceDto;
import com.geeklabs.attendancesystem.facade.AttendanceFacade;
import com.geeklabs.attendancesystem.repository.AttendanceRepository;

public class AttendanceFacadeImpl implements AttendanceFacade {
	
	private DozerBeanMapper dozerBeanMapper;
	private AttendanceRepository attendanceRepository;
	
	public void setDozerBeanMapper(DozerBeanMapper dozerBeanMapper) {
		this.dozerBeanMapper = dozerBeanMapper;
	}
	
	public void setAttendanceRepository(
			AttendanceRepository attendanceRepository) {
		this.attendanceRepository = attendanceRepository;
	}
	
	@Override
	public Attendance convertAttendanceDtoToAttendanceDomain(AttendanceDto attendanceDto) {
		Attendance attendance = dozerBeanMapper.map(attendanceDto, Attendance.class);
		attendance.setFaculty(Faculty.key(attendanceDto.getFacultyId()));
		attendance.setStudent(Student.key(attendanceDto.getStudentId()));
		/*Attendance attendance = new Attendance();
		attendance.setBranch(attendanceDto.getBranch());
		attendance.setDate(attendanceDto.getDate());
		attendance.setTime(attendanceDto.getTime());
		attendance.setYear(attendanceDto.getYear());
		attendance.setAttendanceStatus(attendanceDto.isAttendanceStatus());*/
		return attendance;
		
		/*Long studentId = attendanceDto.getStudentId();
		String time = attendanceDto.getTime();
		String date = attendanceDto.getDate();
		boolean attendanceStatus = attendanceDto.isAttendanceStatus();
		boolean attendanceExist = attendanceRepository.isAttendanceExist(studentId, date, time, attendanceStatus);
		if (attendanceExist) {
				return null;
		} else {
			
		}*/
	}

	@Override
	public AttendanceDto convertAttedanceDomainToAttendanceDto(Attendance attendance) {
		AttendanceDto attendanceDto = new AttendanceDto();
		attendanceDto = dozerBeanMapper.map(attendance, AttendanceDto.class);
		attendanceDto.setFacultyId(attendance.getFaculty().getId());
		attendanceDto.setStudentId(attendance.getStudent().getId());
		return attendanceDto;
	}
	
}
