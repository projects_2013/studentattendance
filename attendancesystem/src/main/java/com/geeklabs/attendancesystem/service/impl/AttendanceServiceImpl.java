package com.geeklabs.attendancesystem.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.geeklabs.attendancesystem.domain.Attendance;
import com.geeklabs.attendancesystem.domain.Student;
import com.geeklabs.attendancesystem.dto.AttendanceDto;
import com.geeklabs.attendancesystem.facade.AttendanceFacade;
import com.geeklabs.attendancesystem.repository.AttendanceRepository;
import com.geeklabs.attendancesystem.repository.StudentRepository;
import com.geeklabs.attendancesystem.service.AttendanceService;
import com.geeklabs.attendancesystem.service.EmailService;
import com.geeklabs.attendancesystem.service.SMSService;

public class AttendanceServiceImpl implements AttendanceService, InitializingBean {
	
	private AttendanceRepository attendanceRepository;
	private AttendanceFacade attendanceFacade;
	private StudentRepository studentRepository;

	public void setAttendanceRepository(AttendanceRepository attendanceRepository) {
		this.attendanceRepository = attendanceRepository;
	}
	
	public void setAttendanceFacade(AttendanceFacade attendanceFacade) {
		this.attendanceFacade = attendanceFacade;
	}
	
	public void setStudentRepository(StudentRepository studentRepository) {
		this.studentRepository = studentRepository;
	}
	@Override
	@Transactional
	public void saveAttendance(List<AttendanceDto> attendanceDtos) {
		ArrayList<Attendance> attendanceList = new ArrayList<Attendance>();
		for (AttendanceDto attendanceDto : attendanceDtos) {
			Student student = studentRepository.findOne(attendanceDto.getStudentId());
			boolean attendanceStatus = attendanceDto.isAttendanceStatus();
			if (!attendanceStatus) {
				SMSService smsService = new SMSServiceImpl();
				if (student.getStudentMobileNumber().contains("+91")) {
					smsService.send("attendance status: absent", student.getStudentMobileNumber());
					smsService.send("attendance status: absent", student.getParentMobileNumber());
				}else {
					smsService.send("attendance status: absent", "+91"+student.getStudentMobileNumber());
					smsService.send("attendance status: absent", "+91"+student.getParentMobileNumber());
				}
				
				EmailService emailServiceImpl = new EmailServiceImpl();
				emailServiceImpl.send(student.getStudentEmail(), "about attendance", "Attendance status: /n/n absent on"+attendanceDto.getDate()+"and"+ attendanceDto.getTime());
				emailServiceImpl.send(student.getParentEmail(), "about attendance", "Attendance status about your child /n/nabsent on"+attendanceDto.getDate()+"and"+ attendanceDto.getTime());
			} 
			
		Attendance attendance = attendanceFacade.convertAttendanceDtoToAttendanceDomain(attendanceDto);
		if (attendance != null) {
			attendanceList.add(attendance);
			}
		}
		attendanceRepository.save(attendanceList);
	}

	@Override
	public void sampleTest() {
		
	}
	
	@Override
	public void afterPropertiesSet() throws Exception {
		Assert.notNull(attendanceRepository, "repo instance variable should not be null");
	}

	@Override
	@Transactional
	public List<AttendanceDto> getAttendanceByBranchAndYear(String branch, String year) {
		 List<Attendance> attendanceByBranchAndYear = attendanceRepository.getAttendanceByBranchAndYear(branch, year);
		 List<AttendanceDto> attendanceDtos = new ArrayList<AttendanceDto>();
		 for (Attendance attendance : attendanceByBranchAndYear) {
			 AttendanceDto convertAttedanceDomainToAttendanceDto = attendanceFacade.convertAttedanceDomainToAttendanceDto(attendance);
			 attendanceDtos.add(convertAttedanceDomainToAttendanceDto);
		}
		 return attendanceDtos;
	}

	@Override
	@Transactional
	public List<AttendanceDto> getAttendanceForSpecificStudent(String studentId) {
		 List<Attendance> attendanceForSpecificStudent = attendanceRepository.getAttendanceForSpecificStudent(studentId);
		 List<AttendanceDto> attendanceDtos = new ArrayList<AttendanceDto>();
		 for (Attendance attendance : attendanceForSpecificStudent) {
			 AttendanceDto convertAttedanceDomainToAttendanceDto = attendanceFacade.convertAttedanceDomainToAttendanceDto(attendance);
			 attendanceDtos.add(convertAttedanceDomainToAttendanceDto);
		}
		return attendanceDtos;
	}
}
