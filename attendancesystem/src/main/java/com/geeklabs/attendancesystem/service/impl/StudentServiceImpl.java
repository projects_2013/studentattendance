package com.geeklabs.attendancesystem.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.dozer.DozerBeanMapper;
import org.springframework.transaction.annotation.Transactional;

import com.geeklabs.attendancesystem.domain.Student;
import com.geeklabs.attendancesystem.dto.StudentDto;
import com.geeklabs.attendancesystem.repository.StudentRepository;
import com.geeklabs.attendancesystem.service.StudentService;
import com.geeklabs.attendancesystem.util.ResponseStatus;

public class StudentServiceImpl implements StudentService {

	private StudentRepository studentRepository;
	private DozerBeanMapper dozerBeanMapper;

	public void setStudentRepository(StudentRepository studentRepository) {
		this.studentRepository = studentRepository;
	}

	public void setDozerBeanMapper(DozerBeanMapper dozerBeanMapper) {
		this.dozerBeanMapper = dozerBeanMapper;
	}

	@Override
	@Transactional
	public ResponseStatus saveStudent(Student student) {
		ResponseStatus responseStatus = new ResponseStatus();
		// check whether student existed or not
		Student studentByEmail = studentRepository.getStudentByEmail(student.getStudentEmail());
		if (studentByEmail != null) {
			responseStatus.setStatus("Student Already Existed");
			return responseStatus;
		}
		studentRepository.save(student);
		responseStatus.setStatus("success");
		return responseStatus;
	}

	@Override
	@Transactional
	public List<StudentDto> getStudents(String branch, String year) {
		List<Student> studentsByBranchAndYear = studentRepository.getStudentsByBranchAndYear(branch, year);
		List<StudentDto> studentDtos = new ArrayList<StudentDto>();
		for (Student student: studentsByBranchAndYear) {
			StudentDto studentDto = dozerBeanMapper.map(student, StudentDto.class);
			studentDtos.add(studentDto);
		}
		return studentDtos;
	}
}
