package com.geeklabs.attendancesystem.service;

import com.geeklabs.attendancesystem.domain.Admin;
import com.geeklabs.attendancesystem.util.ResponseStatus;

public interface AdminService {
	ResponseStatus saveAdmin(Admin admin);
}
