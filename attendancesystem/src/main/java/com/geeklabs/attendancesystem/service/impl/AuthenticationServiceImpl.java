package com.geeklabs.attendancesystem.service.impl;

import java.io.IOException;

import org.springframework.transaction.annotation.Transactional;

import com.geeklabs.attendancesystem.domain.Admin;
import com.geeklabs.attendancesystem.domain.Faculty;
import com.geeklabs.attendancesystem.domain.Student;
import com.geeklabs.attendancesystem.dto.LoginAs;
import com.geeklabs.attendancesystem.repository.AdminRepository;
import com.geeklabs.attendancesystem.repository.FacultyRepository;
import com.geeklabs.attendancesystem.repository.StudentRepository;
import com.geeklabs.attendancesystem.service.AuthenticationService;
import com.geeklabs.attendancesystem.util.ResponseStatus;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson.JacksonFactory;
import com.google.api.services.oauth2.Oauth2;
import com.google.api.services.oauth2.model.Userinfo;

public class AuthenticationServiceImpl implements AuthenticationService {
	private static final HttpTransport HTTP_TRANSPORT = new NetHttpTransport();
	private static final JsonFactory FACTORY = new JacksonFactory();
	private FacultyRepository facultyRepository;
	private StudentRepository studentRepository;
	private AdminRepository adminRepository;

	public void setFacultyRepository(FacultyRepository facultyRepository) {
		this.facultyRepository = facultyRepository;
	}

	public void setAdminRepository(AdminRepository adminRepository) {
		this.adminRepository = adminRepository;
	}

	public void setStudentRepository(StudentRepository studentRepository) {
		this.studentRepository = studentRepository;
	}

	@Override
	@Transactional
	public ResponseStatus authenticateGivenAuthToken(LoginAs loginAs) {
		ResponseStatus responseStatus = new ResponseStatus();
		Userinfo userInfo = getUserInfo(loginAs.getAuthToken());
		if (!userInfo.containsKey("error")) {
			String email = userInfo.getEmail();
			String loginType = loginAs.getLoginType();

			// get authenticated user by authtype
			if (loginType.equals("Faculty")) {
				Faculty facultyByEmail = facultyRepository.getFacultyByEmail(email);
				// check user already exist also sign in status
				if (facultyByEmail != null) {
					if (facultyByEmail.isSignInStatus()) {
						responseStatus.setStatus("SigninStatus");
						return responseStatus;
					} else {
						facultyByEmail.setSignInStatus(true);
						facultyRepository.save(facultyByEmail);
						responseStatus.setStatus("Faculty");
						responseStatus.setName(facultyByEmail.getName());
						responseStatus.setId(facultyByEmail.getId());
						return responseStatus;
					}
				} 
				return responseStatus;
			} else if (loginType.equals("Student")) {
				// check user already exist also sign in status
				Student studentByEmail = studentRepository.getStudentByEmail(email);
				if (studentByEmail != null) {
					if (studentByEmail.isSignInStatus()) {
						responseStatus.setStatus("SigninStatus");
						return responseStatus;
					} else {
						studentByEmail.setSignInStatus(true);
						studentRepository.save(studentByEmail);
						responseStatus.setStatus("Student");
						responseStatus.setName(studentByEmail.getName());
						responseStatus.setId(studentByEmail.getId());
						responseStatus.setBranch(studentByEmail.getBranch());
						responseStatus.setYear(studentByEmail.getYear());
						return responseStatus;
					}
				}
				return responseStatus;
			} else if (loginType.equals("Admin")) {
				// check user already exist also sign in status
				 Admin adminByEmail = adminRepository.getAdminByEmail(email);
				if (adminByEmail != null) {
					if (adminByEmail.isSignInStatus()) {
						responseStatus.setStatus("SigninStatus");
						return responseStatus;
					} else {
						adminByEmail.setSignInStatus(true);
						adminRepository.save(adminByEmail);
						responseStatus.setStatus("Admin");
						responseStatus.setName(adminByEmail.getName());
						responseStatus.setId(adminByEmail.getId());
						return responseStatus;
					}
				}
			}
		}
		return responseStatus;
	}

	// get userInfo by google authtoken
	private Userinfo getUserInfo(String authToken) {
		GoogleCredential credential = new GoogleCredential()
				.setAccessToken(authToken);
		Oauth2 oauth2 = new Oauth2.Builder(HTTP_TRANSPORT, FACTORY, credential)
				.build();
		Userinfo userInfo = null;
		try {
			userInfo = oauth2.userinfo().get().setOauthToken(authToken)
					.execute();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return userInfo;
	}

	@Override
	@Transactional
	public ResponseStatus signOut(String id, String loginType) {
		Long userId = Long.valueOf(id);
		ResponseStatus responseStatus = new ResponseStatus();
		if (loginType.equals("Admin")) {
			Admin admin = adminRepository.findOne(userId);
			admin.setSignInStatus(false);
			adminRepository.save(admin);
			responseStatus.setStatus("Signout");
			return responseStatus;
		} else if (loginType.equals("Student")) {
			Student student = studentRepository.findOne(userId);
			student.setSignInStatus(false);
			studentRepository.save(student);
			responseStatus.setStatus("Signout");
			return responseStatus;
		} else if (loginType.equals("Faculty")) {
			Faculty faculty = facultyRepository.findOne(userId);
			faculty.setSignInStatus(false);
			facultyRepository.save(faculty);
			responseStatus.setStatus("Signout");
			return responseStatus;
		}
		return responseStatus;
	}
}
