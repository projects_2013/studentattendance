package com.geeklabs.attendancesystem.service.impl;

import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import org.springframework.transaction.annotation.Transactional;

import com.geeklabs.attendancesystem.domain.Admin;
import com.geeklabs.attendancesystem.repository.AdminRepository;
import com.geeklabs.attendancesystem.service.AdminService;
import com.geeklabs.attendancesystem.util.ResponseStatus;

public class AdminServiceImpl implements AdminService{
	
	private AdminRepository adminRepository;
	
	public void setAdminRepository(AdminRepository adminRepository) {
		this.adminRepository = adminRepository;
	}
	
	@Override
	@Transactional
	public ResponseStatus saveAdmin(Admin admin) {
		// check whether Admin existed or not
				Admin adminByEmail = adminRepository.getAdminByEmail(admin.getEmail());
				ResponseStatus responseStatus = new ResponseStatus();
				if (adminByEmail != null) {
					responseStatus.setStatus("Admin Already Existed");
					return responseStatus;
				}
				adminRepository.save(admin);
				responseStatus.setStatus("success");
				//send sms test
				sendSMS();
				//responseStatus.setId(adminByEmail.getId());
				//responseStatus.setName(adminByEmail.getName());
				return responseStatus;
			}

	private void sendSMS() {
		try {
			String recipient = "9533388032";
			String message = "Hello World from web server";
			String username = "admin";
			String password = "abc123";
			String originator = "06201234567";

			String requestUrl = "http://127.0.0.1:9501/api?action=sendmessage&"
					+ "username=" + URLEncoder.encode(username, "UTF-8")
					+ "&password=" + URLEncoder.encode(password, "UTF-8")
					+ "&recipient=" + URLEncoder.encode(recipient, "UTF-8")
					+ "&messagetype=SMS:TEXT" + "&messagedata="
					+ URLEncoder.encode(message, "UTF-8") + "&originator="
					+ URLEncoder.encode(originator, "UTF-8")
					+ "&serviceprovider=GSMModem1" + "&responseformat=html";

			URL url = new URL(requestUrl);
			HttpURLConnection uc = (HttpURLConnection) url.openConnection();

			System.out.println(uc.getResponseMessage());

			uc.disconnect();

		} catch (Exception ex) {
			System.out.println(ex.getMessage());

		}
	}
}
