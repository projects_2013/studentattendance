package com.geeklabs.attendancesystem.service;

import java.util.List;

import com.geeklabs.attendancesystem.domain.Student;
import com.geeklabs.attendancesystem.dto.StudentDto;
import com.geeklabs.attendancesystem.util.ResponseStatus;

public interface StudentService {
	ResponseStatus saveStudent(Student student);
	List<StudentDto> getStudents(String branch, String faculty);
}
