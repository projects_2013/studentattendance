package com.geeklabs.attendancesystem.service;

import java.util.List;

import com.geeklabs.attendancesystem.domain.Faculty;
import com.geeklabs.attendancesystem.dto.FacultyDto;
import com.geeklabs.attendancesystem.util.ResponseStatus;

public interface FacultyService {
	ResponseStatus saveFaculty(Faculty faculty);
	List<FacultyDto> getFaculties();
}
