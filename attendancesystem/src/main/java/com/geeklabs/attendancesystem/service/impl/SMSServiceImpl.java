package com.geeklabs.attendancesystem.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import com.geeklabs.attendancesystem.service.SMSService;
import com.twilio.sdk.TwilioRestClient;
import com.twilio.sdk.TwilioRestException;
import com.twilio.sdk.resource.factory.MessageFactory;
import com.twilio.sdk.resource.instance.Message;

public class SMSServiceImpl implements SMSService {

	public static final String ACCOUNT_SID = "AC080fd81595f97e73bc2ea409ab9ddbc0"; 
	 public static final String AUTH_TOKEN = "f45f18a117f4285e81b9d9c146a08c54";
	 public static final String FROM = "+18564694127";
	@Override
	public void send(String body, String mobileNumber) {
		TwilioRestClient client = new TwilioRestClient(ACCOUNT_SID, AUTH_TOKEN); 
		 
		 // Build the parameters 
		 List<NameValuePair> params = new ArrayList<NameValuePair>(); 
		 params.add(new BasicNameValuePair("To", mobileNumber)); 
		 params.add(new BasicNameValuePair("From", "+18564694127")); 
		 params.add(new BasicNameValuePair("Body", body));   
	 
		 MessageFactory messageFactory = client.getAccount().getMessageFactory(); 
		 Message message = null;
		try {
			message = messageFactory.create(params);
		} catch (TwilioRestException e) {
			e.printStackTrace();
		} 
		 System.out.println(message.getSid()); 
		 System.out.println(message.getStatus());
		 System.out.println(message.getTo());
	 }

}
