package com.geeklabs.attendancesystem.service;

import com.geeklabs.attendancesystem.dto.LoginAs;
import com.geeklabs.attendancesystem.util.ResponseStatus;

public interface AuthenticationService {
	ResponseStatus authenticateGivenAuthToken(LoginAs loginAs);
	ResponseStatus signOut(String id, String loginType);
}
