package com.geeklabs.attendancesystem.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.dozer.DozerBeanMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.geeklabs.attendancesystem.domain.Faculty;
import com.geeklabs.attendancesystem.dto.FacultyDto;
import com.geeklabs.attendancesystem.repository.FacultyRepository;
import com.geeklabs.attendancesystem.service.FacultyService;
import com.geeklabs.attendancesystem.util.ResponseStatus;

@Service
public class FacultyServiceImpl implements FacultyService{
	
	private FacultyRepository facultyRepository;
	private DozerBeanMapper dozerBeanMapper;
	
	public void setFacultyRepository(FacultyRepository facultyRepository) {
		this.facultyRepository = facultyRepository;
	}
	
	public void setDozerBeanMapper(DozerBeanMapper dozerBeanMapper) {
		this.dozerBeanMapper = dozerBeanMapper;
	}

	@Override
	@Transactional
	public ResponseStatus saveFaculty(Faculty faculty) {
		// check whether faculty existed or not
		ResponseStatus responseStatus = new ResponseStatus();
		Faculty facultyByEmail = facultyRepository.getFacultyByEmail(faculty.getEmail());
		if (facultyByEmail != null) {
			responseStatus.setStatus("Faculty Already Existed");
			return responseStatus;
		}
		facultyRepository.save(faculty);
		responseStatus.setStatus("success");
		return responseStatus;
	}
	
	@Override
	@Transactional(readOnly = true)
	public List<FacultyDto> getFaculties() {
		Iterable<Faculty> facultyIterable = facultyRepository.findAll();
		List<FacultyDto> facultyDtos = new ArrayList<FacultyDto>();
		Iterator<Faculty> iterator = facultyIterable.iterator();
		while (iterator.hasNext()) {
			Faculty faculty = iterator.next();
			FacultyDto facultyDto = dozerBeanMapper.map(faculty, FacultyDto.class);
			facultyDtos.add(facultyDto);
		}
		return facultyDtos;
	}
}
