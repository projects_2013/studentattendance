package com.geeklabs.attendancesystem.service;

public interface SMSService {
	void send(String body, String mobileNumber);
}
