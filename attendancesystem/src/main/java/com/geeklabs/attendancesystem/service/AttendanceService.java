package com.geeklabs.attendancesystem.service;

import java.util.List;

import com.geeklabs.attendancesystem.dto.AttendanceDto;

public interface AttendanceService {
	void saveAttendance(List<AttendanceDto> attendanceDtos);
	void sampleTest();
	List<AttendanceDto> getAttendanceByBranchAndYear(String branch, String year);
	List<AttendanceDto> getAttendanceForSpecificStudent(String studentId);
}
