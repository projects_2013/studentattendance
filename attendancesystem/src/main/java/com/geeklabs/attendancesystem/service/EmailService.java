package com.geeklabs.attendancesystem.service;
public interface EmailService {
	void send(String toEmail, String subject, String body);
}