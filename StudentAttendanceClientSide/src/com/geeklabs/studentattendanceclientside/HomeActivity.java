package com.geeklabs.studentattendanceclientside;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.geeklabs.studentattendanceclientside.oauth.AuthActivity;
import com.geeklabs.studentattendanceclientside.preferences.AuthPreferences;
import com.geeklabs.studentattendanceclientside.util.NetworkService;

public class HomeActivity extends Activity {

	private Spinner spinner;
	private String name;
	private AuthPreferences authPreferences;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		authPreferences = new AuthPreferences(this);
		setContentView(R.layout.home);
		addItemsOnBranchSpinner();
	}

	private void addItemsOnBranchSpinner() {

		spinner = (Spinner) findViewById(R.id.login_type_spinner);
		List<String> list = new ArrayList<String>();
		list.add("Admin");
		list.add("Faculty");
		list.add("Student");
		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, list);
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner.setAdapter(dataAdapter);
		spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				name = spinner.getSelectedItem().toString();
				//Toast.makeText(getApplicationContext(), name, Toast.LENGTH_LONG).show();
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {

			}
		});

		findViewById(R.id.go_button1).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// Check for n/w connection
				if (!NetworkService
						.isNetWorkAvailable(HomeActivity.this)) {
					AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
							HomeActivity.this);
					// set title
					alertDialogBuilder.setTitle("Warning");
					alertDialogBuilder
							.setMessage("Check your network connection and try again.");
					// set dialog message
					alertDialogBuilder
							.setCancelable(false)
							.setNegativeButton(
									"Ok",
									new DialogInterface.OnClickListener() {
										public void onClick(
												DialogInterface dialog,
												int id) {
										}
									});
					// create alert dialog
					AlertDialog alertDialog = alertDialogBuilder.create();

					// show it
					alertDialog.show();
				} else {
					Intent intent = new Intent(getApplicationContext(), AuthActivity.class);
					intent.putExtra("LoginAs", name);
					startActivity(intent);
				}
			}
		});
		String loginType = authPreferences.getLoginType();
		if (authPreferences.isUserSignedIn() && authPreferences.getUserAccount() != null && loginType.contains("Admin")) {
			Intent i = new Intent(getApplicationContext(), AdminHomeActivity.class);
			startActivity(i);
		}else if (authPreferences.isUserSignedIn() && authPreferences.getUserAccount() != null && loginType.contains("Faculty")) {
			Intent i = new Intent(getApplicationContext(), FacultyActivity.class);
			startActivity(i);
		}else if (authPreferences.isUserSignedIn() && authPreferences.getUserAccount() != null && loginType.contains("Student")) {
			Intent i = new Intent(getApplicationContext(), StudentActivity.class);
			startActivity(i);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.home, menu);
		return true;
	}
	
	@Override
	public void onBackPressed() {
		moveTaskToBack(true);
	}

}
