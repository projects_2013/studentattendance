package com.geeklabs.studentattendanceclientside;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.geeklabs.studentattendanceclientside.util.SIgnOutUtil;

public class AdminHomeActivity extends Activity {

	private Intent intent;
	private Button createUserButton, feeDetailsButton;
	private String adminName;
	private long adminId;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.admin_selected_details);
		intent = getIntent();
		adminName = intent.getStringExtra("name");
		adminId = intent.getLongExtra("id", 1);
		createUserButton = (Button) findViewById(R.id.admin_select_details_create_user);
		feeDetailsButton = (Button) findViewById(R.id.admin_select_details_fee_details);
//		resultsButton = (Button) findViewById(R.id.admin_select_details_results);
		
		onClickOnCreateUserButton();
		onClickOnFeeDetailsButton();
	}

	private void onClickOnCreateUserButton() {
		createUserButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// open activity to create user
				
				intent = new Intent(AdminHomeActivity.this, AdminActivity.class);
				intent.putExtra("name", adminName);
				intent.putExtra("id", adminId);
				startActivity(intent);
			}
		});
	}
	
	private void onClickOnFeeDetailsButton() {
		feeDetailsButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				//get student details and open activity to create student fee details 
				intent = new Intent(AdminHomeActivity.this, HODSelectStudentsActivity.class);
				intent.putExtra("name", adminName);
				intent.putExtra("id", adminId);
				startActivity(intent);
			}
		});
	}

	
	@SuppressLint("NewApi")
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add("signout").setIcon(R.drawable.sign_out).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		return super.onCreateOptionsMenu(menu);
	}
	
	
	@Override
	public boolean onOptionsItemSelected(android.view.MenuItem item) {
		if(item.getTitle().equals("signout")){
			//create progress Dialog
			final ProgressDialog progressDialog = new ProgressDialog(AdminHomeActivity.this);
			progressDialog.setMessage("signout is in progress...");
			Log.i("signOut process", "saving attendance is in progress");
			progressDialog.setIndeterminate(true);
			progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progressDialog.show();
			
			SIgnOutUtil.signOut(progressDialog, getApplicationContext());
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		moveTaskToBack(true);
	}
}
