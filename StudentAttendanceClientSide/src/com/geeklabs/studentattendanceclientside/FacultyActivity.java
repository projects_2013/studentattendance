package com.geeklabs.studentattendanceclientside;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.actionbarsherlock.view.MenuItem;
import com.geeklabs.studentattendanceclientside.communication.task.get.GetStudentsTask;
import com.geeklabs.studentattendanceclientside.domain.UtilDomain;
import com.geeklabs.studentattendanceclientside.util.SIgnOutUtil;

public class FacultyActivity extends Activity {

	private Spinner branchSpinner, yearSpinner;
	private String name;
	private TextView date, time;
	private Intent intent;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.select_details);
		intent = getIntent();
		goButtonAction();
		addItemsOnBranchSpinner();
		addItemsOnYearSpinner();
		setCurrentDateTime();
	}

	private void setCurrentDateTime() {
		// get Id's
		date = (TextView) findViewById(R.id.date);
		time = (TextView) findViewById(R.id.time);
		// set current date and time
		date.setText(getCurrentDate());
		time.setText(getCurrentTime());

	}

	private void addItemsOnBranchSpinner() {
		branchSpinner = (Spinner) findViewById(R.id.faculty_select_branch_spinner);
		List<String> list = new ArrayList<String>();
		list.add("CSE");
		list.add("IT");
		list.add("ECE");
		list.add("EEE");
		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, list);
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		branchSpinner.setAdapter(dataAdapter);
		branchSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				name = branchSpinner.getSelectedItem().toString();
				//Toast.makeText(getApplicationContext(), name, Toast.LENGTH_LONG).show();
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {

			}
		});
	}

	private void addItemsOnYearSpinner() {
		yearSpinner = (Spinner) findViewById(R.id.faculty_select_year_spinner);
		List<String> list = new ArrayList<String>();
		list.add("First");
		list.add("Second");
		list.add("Third");
		list.add("Fourth");
		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, list);
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		yearSpinner.setAdapter(dataAdapter);
		yearSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				name = yearSpinner.getSelectedItem().toString();
				Toast.makeText(getApplicationContext(), name, Toast.LENGTH_LONG).show();
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {

			}
		});
	}

	private void goButtonAction() {

		findViewById(R.id.faculty_go_button1).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				
				String branch = branchSpinner.getSelectedItem().toString();
				String year = yearSpinner.getSelectedItem().toString();
				//create progressDialog
				final ProgressDialog progressDialog = new ProgressDialog(FacultyActivity.this);
				progressDialog.setMessage("Retrieving students list...");
				progressDialog.setCancelable(false);
				// call async task to get the students
				String facultyName = intent.getStringExtra("name");
				UtilDomain utilDomain = new UtilDomain();
				utilDomain.setFacultyName(facultyName);
				utilDomain.setBranch(branch);
				utilDomain.setYear(year);
				utilDomain.setDate(getCurrentDate());
				utilDomain.setTime(getCurrentTime());
				//get FacultyId
				long facultyId = intent.getLongExtra("id", 1);
				utilDomain.setFacultyId(facultyId);
				GetStudentsTask getStudentsTask = new GetStudentsTask(FacultyActivity.this, progressDialog, utilDomain);
				getStudentsTask.execute();
			}
		});
	}

	@SuppressLint("SimpleDateFormat")
	public String getCurrentDate() {
		Calendar cal = Calendar.getInstance();
		cal.getTime();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		return sdf.format(cal.getTime());
	}

	@SuppressLint("SimpleDateFormat")
	public String getCurrentTime() {
		Calendar cal = Calendar.getInstance();
		cal.getTime();
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		return sdf.format(cal.getTime());

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.faculty, menu);
		menu.add("Sign Out").setTitle("Sign out").setIcon(R.drawable.sign_out).setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(android.view.MenuItem item) {
		if (item.getTitle().equals("Sign out")) {
			//create progress Dialog
			final ProgressDialog progressDialog = new ProgressDialog(this);
			progressDialog.setMessage("signOut is in progress...");
			Log.i("signOut process", "saving attendance is in progress");
			progressDialog.setIndeterminate(true);
			progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progressDialog.show();
			//signOut the user
			SIgnOutUtil.signOut(progressDialog, getApplicationContext());
			return true;
	}
		return false;
}
}
