package com.geeklabs.studentattendanceclientside.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.geeklabs.studentattendanceclientside.R;
import com.geeklabs.studentattendanceclientside.domain.Student;

public class StudentListCustomAdapter extends BaseAdapter {
	private LayoutInflater inflater;
	private List<Student> students = new ArrayList<Student>();
	private Student student;
	private Context context;
	public void setAttendanceDtos(List<Student> students) {
		if (students.size() > 0) {
			this.students.clear();
			this.students.addAll(students);
		}
	}

	public List<Student> getStudents() {
		return students;
	}

	public StudentListCustomAdapter(Context context) {
		this.context = context;
		inflater = LayoutInflater.from(context);
		student = new Student();
	}

	@Override
	public int getCount() {
		return students.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		student = students.get(position);
		if (convertView == null) {
			inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.custom_student_list, null);
			holder = new ViewHolder();
			holder.studentNameTextView = (TextView) convertView.findViewById(R.id.student_name_textview);
			holder.studentNameTextView.setText(student.getName());
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		return convertView;
	}

	private static class ViewHolder {
		TextView HTNO, studentNameTextView;
	}
}
