package com.geeklabs.studentattendanceclientside.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;

import com.geeklabs.studentattendanceclientside.R;
import com.geeklabs.studentattendanceclientside.domain.AttendanceDto;
import com.geeklabs.studentattendanceclientside.preferences.AuthPreferences;

public class CustomAdapter extends BaseAdapter {
	private LayoutInflater inflater;
	private List<AttendanceDto> attendanceDtos = new ArrayList<AttendanceDto>();
	private AttendanceDto attendanceDto;
	private Context context;
	private int index;
	private AuthPreferences authPreferences;

	public void setAttendanceDtos(List<AttendanceDto> attendanceDtos) {
		if (attendanceDtos.size() > 0) {
			this.attendanceDtos.clear();
			this.attendanceDtos.addAll(attendanceDtos);
			authPreferences = new AuthPreferences(context);
		}
	}

	public List<AttendanceDto> getAttendanceDtos() {
		return attendanceDtos;
	}

	public CustomAdapter(Context context) {
		inflater = LayoutInflater.from(context);
		attendanceDto = new AttendanceDto();
		this.context = context;
	}

	@Override
	public int getCount() {
		return attendanceDtos.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int index, View convertView, ViewGroup parent) {
		attendanceDto = attendanceDtos.get(index);
		final ViewHolder holder;
		this.index = index;
		if (convertView == null) {
			/*
			 * inflater = (LayoutInflater) context
			 * .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			 */
			convertView = inflater.inflate(R.layout.custom_row, null);
			holder = new ViewHolder();
			holder.tname = (TextView) convertView
					.findViewById(R.id.textViewName);
			holder.tname.setText(String.valueOf(attendanceDto.getStudentName()));
			holder.checkBox = (CheckBox) convertView
					.findViewById(R.id.CheckBox);
			// set AttendanceStatus
			boolean attendanceStatus = attendanceDto.isAttendanceStatus();
			if (attendanceStatus) {
				holder.checkBox.setChecked(true);
			} else {
				holder.checkBox.setChecked(false);
			}

			// change attendance status after clicking checkbox
			/*
			 * holder.checkBox.setOnClickListener(new OnClickListener() {
			 * 
			 * @Override public void onClick(View v) { if
			 * (!attendanceDto.isAttendanceStatus()) {
			 * attendanceDto.setAttendanceStatus(true);
			 * holder.checkBox.setChecked(true); } else {
			 * attendanceDto.setAttendanceStatus(false);
			 * holder.checkBox.setChecked(false); } } });
			 */
			holder.checkBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
				
				private int position = CustomAdapter.this.index;
				
						@Override
						public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
							holder.innerPosition = position;
							if (new AuthPreferences(context).getLoginType().equals("Faculty") && isChecked) {
								attendanceDto = attendanceDtos.get(position);
								attendanceDto.setAttendanceStatus(true);
								attendanceDtos.set(holder.innerPosition , attendanceDto);
								holder.checkBox.setChecked(true);
							} else if (new AuthPreferences(context).getLoginType().equals("Faculty") && !isChecked) {
								attendanceDto = attendanceDtos.get(position);
								attendanceDto.setAttendanceStatus(false);
								attendanceDtos.set(holder.innerPosition , attendanceDto);
								holder.checkBox.setChecked(false);
							}
						}
				
					});

			/*
			 * holder.checkBox.setOnTouchListener(new OnTouchListener() {
			 * 
			 * @Override public boolean onTouch(View v, MotionEvent event) {
			 * 
			 * if (!attendanceDto.isAttendanceStatus()) {
			 * attendanceDto.setAttendanceStatus(true);
			 * holder.checkBox.setChecked(true); } else {
			 * attendanceDto.setAttendanceStatus(false);
			 * holder.checkBox.setChecked(false); } return false; } });
			 */

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		return convertView;
	}

	private static class ViewHolder {
		TextView tname;
		CheckBox checkBox;
		private int innerPosition;
	}
}
