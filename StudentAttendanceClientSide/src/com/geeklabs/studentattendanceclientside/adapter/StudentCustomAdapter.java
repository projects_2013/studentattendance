package com.geeklabs.studentattendanceclientside.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.geeklabs.studentattendanceclientside.R;
import com.geeklabs.studentattendanceclientside.domain.Attendance;

public class StudentCustomAdapter extends BaseAdapter {
	private LayoutInflater inflater;
	private List<Attendance> attendances = new ArrayList<Attendance>();
	private Attendance attendance;
	private Context context;
	public void setAttendances(List<Attendance> attendances) {
		this.attendances.clear();
		this.attendances = attendances;
	}
	
	public List<Attendance> getAttendances() {
		return attendances;
	}

	public StudentCustomAdapter(Context context) {
		inflater = LayoutInflater.from(context);
		this.context = context;
		attendance = new Attendance();
	}
	@Override
	public int getCount() {
		return attendances.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		attendance = attendances.get(position);
		if (convertView == null) {
			inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.student_custom_row, null);
			holder = new ViewHolder();
			//set views to Holder
			holder.checkBox = (CheckBox) convertView.findViewById(R.id.student_checkBox);
			holder.dateTextView = (TextView) convertView.findViewById(R.id.dateTextView);
			holder.dateTextView.setText(attendance.getDate());
			holder.timeTextView = (TextView) convertView.findViewById(R.id.timetextView);
			holder.timeTextView.setText(attendance.getTime());
			
			// set AttendanceStatus
			boolean attendanceStatus = attendance.isAttendanceStatus();
				if (attendanceStatus) {
				holder.checkBox.setChecked(true);
				} else {
				holder.checkBox.setChecked(false);
			}
		}else {
			holder = (ViewHolder) convertView.getTag();
		}
		return convertView;
	}
	
	private static class ViewHolder {
		TextView timeTextView, dateTextView;
		CheckBox checkBox;
	}
}
