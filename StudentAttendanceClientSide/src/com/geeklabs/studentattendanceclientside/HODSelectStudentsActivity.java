package com.geeklabs.studentattendanceclientside;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.actionbarsherlock.view.MenuItem;
import com.geeklabs.studentattendanceclientside.communication.task.get.GetStudentsTask;
import com.geeklabs.studentattendanceclientside.domain.UtilDomain;
import com.geeklabs.studentattendanceclientside.util.SIgnOutUtil;

public class HODSelectStudentsActivity extends Activity {
	private Spinner branchSpinner, yearSpinner;

	// private Button btnSubmit;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.hod_select_details);
		addItemsOnBranchSpinner();
		addItemsOnYearSpinner();
		findViewById(R.id.go_button).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				
				//get students by branch and year
				String branch = branchSpinner.getSelectedItem().toString();
				String year = yearSpinner.getSelectedItem().toString();
				
				//create progressDialog
				final ProgressDialog progressDialog = new ProgressDialog(HODSelectStudentsActivity.this);
				progressDialog.setMessage("please wait a min... :) ");
				progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
				progressDialog.setIndeterminate(true);
				progressDialog.show();
				
				// create utilDomain object
				UtilDomain utilDomain = new UtilDomain();
				utilDomain.setBranch(branch);
				utilDomain.setYear(year);
				
				GetStudentsTask getStudentsTask = new GetStudentsTask(HODSelectStudentsActivity.this, progressDialog, utilDomain);
				getStudentsTask.execute();
			}
		});
	}

	private void addItemsOnBranchSpinner() {

		branchSpinner = (Spinner) findViewById(R.id.branch_spinner);
		List<String> list = new ArrayList<String>();
		list.add("CSE");
		list.add("IT");
		list.add("ECE");
		list.add("EEE");
		list.add("MECH");
		list.add("CIVIL");
		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, list);
		dataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		branchSpinner.setAdapter(dataAdapter);
	}

	private void addItemsOnYearSpinner() {

		yearSpinner = (Spinner) findViewById(R.id.year_spinner);
		List<String> list = new ArrayList<String>();
		list.add("First");
		list.add("Second");
		list.add("Third");
		list.add("Fourth");
		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, list);
		dataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		yearSpinner.setAdapter(dataAdapter);
	}
	
	@SuppressLint("NewApi")
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.faculty, menu);
		menu.add("Sign Out").setTitle("Sign out").setIcon(R.drawable.sign_out).setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(android.view.MenuItem item) {
		if (item.getTitle().equals("Sign out")) {
			//create progress Dialog
			final ProgressDialog progressDialog = new ProgressDialog(this);
			progressDialog.setMessage("signut is in progress...");
			Log.i("signout process", "saving attendance is in progress");
			progressDialog.setIndeterminate(true);
			progressDialog.setCancelable(false);
			progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progressDialog.show();
			//signOut the user
			SIgnOutUtil.signOut(progressDialog, getApplicationContext());
			return true;
	}
		return false;
	}
}
