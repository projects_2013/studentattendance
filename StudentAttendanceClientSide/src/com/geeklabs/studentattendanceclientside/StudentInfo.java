package com.geeklabs.studentattendanceclientside;

import com.actionbarsherlock.view.MenuItem;
import com.geeklabs.studentattendanceclientside.communication.task.get.GetStudentAttendanceTask;
import com.geeklabs.studentattendanceclientside.util.SIgnOutUtil;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class StudentInfo extends Activity {

	private Intent intent;
	private Button button;
	private String sname;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.student_info);
		intent = getIntent();
		sname = intent.getStringExtra("name");
		TextView studentNameTextView = (TextView) findViewById(R.id.student_name);
		studentNameTextView.setText(sname);
		button = (Button) findViewById(R.id.button_get_specific_student_attendance);

		// button click get attendance results
		getAttendanceForspecificStudent();
	}

	private void getAttendanceForspecificStudent() {
		button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// create progress Dialog
				final ProgressDialog progressDialog = new ProgressDialog(
						StudentInfo.this);
				progressDialog.setMessage("signOut is in progress...");
				Log.i("signOut process", "saving attendance is in progress");
				progressDialog.setIndeterminate(true);
				progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
				progressDialog.show();
				// call async task
				GetStudentAttendanceTask getStudentAttendanceTask = new GetStudentAttendanceTask(
						progressDialog, StudentInfo.this, sname);
				getStudentAttendanceTask.execute();
			}
		});
	}

	@SuppressLint("NewApi")
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add("SignOut").setTitle("Signout").setIcon(R.drawable.sign_out)
				.setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(android.view.MenuItem item) {
		if (item.getTitle().equals("Sign out")) {
			// create progress Dialog
			final ProgressDialog progressDialog = new ProgressDialog(this);
			progressDialog.setMessage("signOut is in progress...");
			Log.i("signOut process", "saving attendance is in progress");
			progressDialog.setIndeterminate(true);
			progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progressDialog.show();
			// signOut the user
			SIgnOutUtil.signOut(progressDialog, getApplicationContext());
			return true;
		}
		return false;
	}
}
