package com.geeklabs.studentattendanceclientside;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.view.MenuItem;
import com.geeklabs.studentattendanceclientside.adapter.CustomAdapter;
import com.geeklabs.studentattendanceclientside.communication.task.post.SaveAttendanceTask;
import com.geeklabs.studentattendanceclientside.domain.Attendance;
import com.geeklabs.studentattendanceclientside.domain.AttendanceDto;
import com.geeklabs.studentattendanceclientside.domain.Student;
import com.geeklabs.studentattendanceclientside.domain.UtilDomain;
import com.geeklabs.studentattendanceclientside.util.DataWrapper;
import com.geeklabs.studentattendanceclientside.util.SIgnOutUtil;

public class StudentActivity extends ListActivity {

	private CustomAdapter adapter;
	private Intent intent;
	private UtilDomain utilDomain;
	private List<Student> studentList;
	private TextView facultyTextView,branchTextView,timeTextView,dateTextView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.attendance);
		facultyTextView = (TextView) findViewById(R.id.f_name);
		branchTextView = (TextView) findViewById(R.id.branch);
		timeTextView = (TextView) findViewById(R.id.time1);
		dateTextView = (TextView) findViewById(R.id.date1);
		adapter = new CustomAdapter(getApplicationContext());
		intent = getIntent();
		studentList = getStudentList();
		utilDomain = getUtilData();
		setTextViews();
		addStudentInfoToAttendanceDto();
		setListAdapter(adapter);
		
	}
	
	private void addStudentInfoToAttendanceDto() {
		List<AttendanceDto> attendanceDtos = new ArrayList<AttendanceDto>();
		if (!studentList.isEmpty()) {
			for (Student student : studentList){
				AttendanceDto attendanceDto = new AttendanceDto();
				attendanceDto.setStudentName(student.getName());
				attendanceDto.setStudentId(student.getId());
				attendanceDtos.add(attendanceDto);
			}
			if (adapter != null && adapter.getAttendanceDtos().isEmpty()) {
				adapter.setAttendanceDtos(attendanceDtos);
			}
		}else {
			Toast.makeText(getApplicationContext(), "No students for this batch", Toast.LENGTH_LONG).show();
		}
	}

	private void setTextViews() {
		facultyTextView.setText(utilDomain.getFacultyName());
		branchTextView.setText(utilDomain.getBranch());
		timeTextView.setText(utilDomain.getTime());
		dateTextView.setText(utilDomain.getDate());
	}

	private UtilDomain getUtilData() {
		 utilDomain = (UtilDomain)intent.getSerializableExtra("com.geeklabs.studentattendanceclientside.utildata");
		 if (utilDomain != null) {
			 return utilDomain;
		} 
		return utilDomain;
	}
	
	

	@SuppressWarnings("unchecked")
	private List<Student> getStudentList() {
		DataWrapper dataWrapper = (DataWrapper)intent.getSerializableExtra("com.geeklabs.studentattendanceclientside.studentlist");
		if (dataWrapper != null) {
			return dataWrapper.getList();
		}
		return new ArrayList<Student>();
	}
	
	@SuppressLint("NewApi")
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add("save").setIcon(R.drawable.save).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		menu.add("SignOut").setTitle("Signout").setIcon(R.drawable.sign_out).setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(android.view.MenuItem item) {
		if (item.getTitle().equals("save")) {
			//create progress Dialog
			final ProgressDialog progressDialog = new ProgressDialog(StudentActivity.this);
			progressDialog.setMessage("saving attendance...");
			Log.i("saving attendance", "saving attendance is in progress");
			progressDialog.setIndeterminate(true);
			progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progressDialog.show();
			
			//create Attendance
			List<AttendanceDto> attendanceDtos = adapter.getAttendanceDtos();
			List<Attendance> attendanceList = new ArrayList<Attendance>();
			for (AttendanceDto attendanceDto : attendanceDtos) {
					Attendance attendance = new Attendance();
					attendance.setBranch(utilDomain.getBranch());
					attendance.setDate(utilDomain.getDate());
					attendance.setFacultyId(utilDomain.getFacultyId());
					attendance.setTime(utilDomain.getTime());
					attendance.setYear(utilDomain.getYear());
					attendance.setStudentId(attendanceDto.getStudentId());
					attendance.setAttendanceStatus(attendanceDto.isAttendanceStatus());
					attendanceList.add(attendance);
			}
			SaveAttendanceTask attendanceTask = new SaveAttendanceTask(progressDialog, StudentActivity.this, attendanceList);
			attendanceTask.execute();
		}else if (item.getTitle().equals("Signout")) {
			//create progress Dialog
			final ProgressDialog progressDialog = new ProgressDialog(this);
			progressDialog.setMessage("signOut is in progress...");
			Log.i("signOut process", "saving attendance is in progress");
			progressDialog.setIndeterminate(true);
			progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progressDialog.show();
			//signOut the user
			SIgnOutUtil.signOut(progressDialog, getApplicationContext());
			return true; 
			
		}
		return super.onOptionsItemSelected(item);
	}
	
}
