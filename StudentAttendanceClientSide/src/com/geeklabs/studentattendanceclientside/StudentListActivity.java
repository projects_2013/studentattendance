package com.geeklabs.studentattendanceclientside;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.geeklabs.studentattendanceclientside.adapter.StudentListCustomAdapter;
import com.geeklabs.studentattendanceclientside.communication.task.get.GetStudentAttendanceTask;
import com.geeklabs.studentattendanceclientside.domain.Student;
import com.geeklabs.studentattendanceclientside.domain.UtilDomain;
import com.geeklabs.studentattendanceclientside.preferences.AuthPreferences;
import com.geeklabs.studentattendanceclientside.util.DataWrapper;
import com.geeklabs.studentattendanceclientside.util.SIgnOutUtil;

public class StudentListActivity extends ListActivity {

	private StudentListCustomAdapter adapter;
	private Intent intent;
	private UtilDomain utilDomain;
	private List<Student> studentList;
//	private TextView studentNameTextView, htNoTextView;
	private ListView listView;
	private AuthPreferences authPreferences;
	private Long studentId;
	private String studentName;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.students_list);
		authPreferences = new AuthPreferences(getApplicationContext());
//		studentNameTextView = (TextView) findViewById(R.id.student_name_textview);
//		htNoTextView = (TextView) findViewById(R.id.htno_textview);
		adapter = new StudentListCustomAdapter(getApplicationContext());
		intent = getIntent();
		studentList = getStudentList();
		utilDomain = getUtilData();
		// setTextViews();
		addStudentInfoToStudentListAdapter();
		setListAdapter(adapter);

		listView = getListView();

		onItemClickListener();
	}

	private void onItemClickListener() {
		// on item click

		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
				Student student = adapter.getStudents().get(position);
				studentId = student.getId();
				studentName = student.getName();
				String loginAs = authPreferences.getLoginType();
				if (loginAs.equals("Admin")) {
					// call a AlertDialog
					callAlertDialogForAdminToChooseOption();

				} 
			}
		});
	}
	
	//Admin alert option
	private void callAlertDialogForAdminToChooseOption() {

		final String items[] = { "Attendance"};
		AlertDialog.Builder alertBuilder = new AlertDialog.Builder(StudentListActivity.this);
		alertBuilder.setTitle("choose option");
		alertBuilder.setItems(items, new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface d, int choice) {
						if (choice == 0) {
							// create progressDialog
							final ProgressDialog progressDialog = new ProgressDialog(StudentListActivity.this);
							progressDialog.setMessage("please wait a min...");
							progressDialog.setCancelable(false);
							progressDialog.show();

							// get attendance specific student
							// attendance
							GetStudentAttendanceTask getStudentAttendanceTask = new GetStudentAttendanceTask(progressDialog, StudentListActivity.this, studentId, studentName);
							getStudentAttendanceTask.execute();
						} 
					}
				});
		alertBuilder.show();
	}
	

	private void addStudentInfoToStudentListAdapter() {
		if (adapter != null && adapter.getStudents().isEmpty()) {
			adapter.setAttendanceDtos(studentList);
			;
		} else {
			Toast.makeText(getApplicationContext(),
					"No students for this batch", Toast.LENGTH_LONG).show();
		}
	}

	private UtilDomain getUtilData() {
		utilDomain = (UtilDomain) intent
				.getSerializableExtra("com.geeklabs.studentattendanceclientside.utildata");
		if (utilDomain != null) {
			return utilDomain;
		}
		return new UtilDomain();
	}

	@SuppressWarnings("unchecked")
	private List<Student> getStudentList() {
		DataWrapper dataWrapper = (DataWrapper) intent
				.getSerializableExtra("com.geeklabs.studentattendanceclientside.studentlist");
		if (dataWrapper != null) {
			return dataWrapper.getList();
		}
		return new ArrayList<Student>();
	}

	@SuppressLint("NewApi")
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add("SignOut").setTitle("Signout").setIcon(R.drawable.sign_out)
				.setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(android.view.MenuItem item) {
		
		  if (item.getTitle().equals("Signout")) {
			// create progress Dialog
			final ProgressDialog progressDialog = new ProgressDialog(this);
			progressDialog.setMessage("signOut is in progress...");
			Log.i("signOut process", "saving attendance is in progress");
			progressDialog.setIndeterminate(true);
			progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progressDialog.show();
			// signOut the user
			SIgnOutUtil.signOut(progressDialog, getApplicationContext());
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
