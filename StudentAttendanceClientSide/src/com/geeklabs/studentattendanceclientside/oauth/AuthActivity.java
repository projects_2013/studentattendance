package com.geeklabs.studentattendanceclientside.oauth;

import java.io.IOException;

import net.frakbot.accounts.chooser.AccountChooser;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import com.geeklabs.studentattendanceclientside.HomeActivity;
import com.geeklabs.studentattendanceclientside.communication.task.post.AuthenticationTask;
import com.geeklabs.studentattendanceclientside.domain.LoginAs;
import com.geeklabs.studentattendanceclientside.preferences.AuthPreferences;

public class AuthActivity extends Activity {

	private static final int AUTHORIZATION_CODE = 1993;
	private static final int ACCOUNT_CODE = 1601;

	private AuthPreferences authPreferences;
	private AccountManager accountManager;
	private Account userAccount;
	private Intent intent;

	/**
	 * change this depending on the scope needed for the things you do in
	 * doCoolAuthenticatedStuff()
	 */

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		/*BitmapDrawable bitmap = (BitmapDrawable) getResources().getDrawable(R.drawable.steel_blue);
		bitmap.setTileModeXY(TileMode.REPEAT, TileMode.REPEAT);
		getSupportActionBar().setBackgroundDrawable(bitmap);*/

		accountManager = AccountManager.get(this);
		authPreferences = new AuthPreferences(this);

		// invalidate old tokens which might be cached. we want a fresh
		// one, which is guaranteed to work
		invalidateToken();

		chooseAccount();
		
		intent = getIntent();
	}

	private void chooseAccount() {
		// using https://github.com/frakbot/Android-AccountChooser for
		// compatibility with older devices
		Intent intent = AccountChooser.newChooseAccountIntent(null, null,
				new String[] { "com.google" }, true, null, null, null, null,
				getApplicationContext());

		startActivityForResult(intent, ACCOUNT_CODE);
	}

	private void requestToken() {
		String user = authPreferences.getUserAccount();
		for (Account account : accountManager.getAccountsByType("com.google")) {
			if (account.name.equals(user)) {
				userAccount = account;
				break;
			}
		}

		accountManager.getAuthToken(userAccount, "oauth2:"
				+ OAuthUserCredStore.SCOPE, null, this, new OnTokenAcquired(),
				null);

	}

	/**
	 * call this method if your token expired, or you want to request a new
	 * token for whatever reason. call requestToken() again afterwards in order
	 * to get a new token.
	 */
	private void invalidateToken() {
		AccountManager accountManager = AccountManager.get(this);
		accountManager.invalidateAuthToken("com.google",
				authPreferences.getAccessToken());

		authPreferences.setAccessToken(null);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (resultCode == RESULT_OK) {
			if (requestCode == AUTHORIZATION_CODE) {
				requestToken();
			} else if (requestCode == ACCOUNT_CODE) {
				String accountName = data
						.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
				authPreferences.setUserAccount(accountName);

				// invalidate old tokens which might be cached. we want a fresh
				// one, which is guaranteed to work
				invalidateToken();

				requestToken();
			}
		} else if (resultCode == RESULT_CANCELED) {
			Intent i = new Intent(this, HomeActivity.class);
			this.startActivity(i);
		}
	}

	private String requestNewToken() {
		AccountManagerFuture<Bundle> accountManagerFuture = accountManager
				.getAuthToken(userAccount,
						"oauth2:" + OAuthUserCredStore.SCOPE, null, this,
						new OnNewTokenAcquired(), null);
		String newAuthToken = null;
		try {
			Bundle bundle = accountManagerFuture.getResult();
			newAuthToken = bundle.getString(AccountManager.KEY_AUTHTOKEN);
		} catch (OperationCanceledException e) {
			e.printStackTrace();
			// If user cancels operation, send back to sign in page
			Intent i = new Intent(AuthActivity.this, HomeActivity.class);
			AuthActivity.this.startActivity(i);
		} catch (AuthenticatorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return newAuthToken;

	}

	private class OnTokenAcquired implements AccountManagerCallback<Bundle> {

		@Override
		public void run(AccountManagerFuture<Bundle> result) {
			try {

				Bundle bundle = result.getResult();

				Intent launch = (Intent) bundle.get(AccountManager.KEY_INTENT);
				if (launch != null) {
					startActivityForResult(launch, AUTHORIZATION_CODE);
				} else {
					String token = bundle
							.getString(AccountManager.KEY_AUTHTOKEN);
					authPreferences.setAccessToken(token);
					invalidateToken();
					AsyncTask<Void, String, String> asyncTask = new AsyncTask<Void, String, String>() {

						@Override
						protected String doInBackground(Void... params) {
							return requestNewToken();
						}

					};
					asyncTask.execute();
					String newToken = asyncTask.get();
					// Register new user
					registerAndValidateUser(newToken);
				}

			} catch (OperationCanceledException canceledException) {
				// If user cancels operation, send back to sign in page
				Intent i = new Intent(AuthActivity.this, HomeActivity.class);
				AuthActivity.this.startActivity(i);
			} catch (Exception e) {
				throw new RuntimeException(e);
			}

		}

	}

	private void registerAndValidateUser(String accessToken) {
		final ProgressDialog signingAPKProgressDialog = new ProgressDialog(
				AuthActivity.this);
		signingAPKProgressDialog
				.setMessage("Login is in process...");
		Log.i("User authentication", "User authentication is in progress");
		signingAPKProgressDialog.setCancelable(false);
		signingAPKProgressDialog.show();
		
		//get the LoginAsType to send to server
		LoginAs loginAs = new LoginAs();
		String loginType = intent.getStringExtra("LoginAs");
		loginAs.setAuthToken(accessToken);
		loginAs.setLoginType(loginType);
		
		//call the aynctask to validate authtoken and user type 
		AuthenticationTask validateUserRunnable = new AuthenticationTask(signingAPKProgressDialog, AuthActivity.this, loginAs);
		validateUserRunnable.execute();
	}
}