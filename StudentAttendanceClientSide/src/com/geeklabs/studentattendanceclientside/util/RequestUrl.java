package com.geeklabs.studentattendanceclientside.util;

public class RequestUrl {
	
	//for local test
	public static final String VALIDATE_TOKEN = "http://192.168.1.120:8908/auth/authToken";
	public static final String SAVE_FACULTY = "http://192.168.1.120:8908/faculty/save";
	public static final String SAVE_STUDENT = "http://192.168.1.120:8908/student/save";
	public static final String GET_STUDENTS = "http://192.168.1.120:8908/student/getStudents/{branch}/{year}";
	public static final String SAVE_ATTENDANCE = "http://192.168.1.120:8908/attendance/save";
	public static final String SIGNOUT = "http://192.168.1.120:8908/auth/signOut/{id}/{loginType}";
	public static final String SPECIFIC_STUDENT_ATTENDANCE = "http://192.168.1.120:8908/attendance/getSingleStudentAttendance/{id}";
	public static String GET_FACULTY = "http://192.168.1.120:8908/faculty/getFaculty";

	
	/*public static final String VALIDATE_TOKEN = "http://sasystemapp.appspot.com/auth/authToken";
	public static final String SAVE_FACULTY = "http://sasystemapp.appspot.com/faculty/save";
	public static final String SAVE_STUDENT = "http://sasystemapp.appspot.com/student/save";
	public static final String GET_STUDENTS = "http://sasystemapp.appspot.com/student/getStudents/{branch}/{year}";
	public static final String SAVE_ATTENDANCE = "http://sasystemapp.appspot.com/attendance/save";
	public static final String SPECIFIC_STUDENT_ATTENDANCE = "http://sasystemapp.appspot.com/attendance/getSingleStudentAttendance/{id}";
	public static final String SIGNOUT = "http://sasystemapp.appspot.com/auth/signOut/{id}/{loginType}";
	public static String GET_FACULTY = "http://sasystemapp.appspot.com/faculty/getFaculty";*/
}

//192.168.1.148    Shylu
//192.168.1.120    Balu
