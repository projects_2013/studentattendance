package com.geeklabs.studentattendanceclientside.util;

import java.io.Serializable;
import java.util.List;

@SuppressWarnings("serial")
public class DataWrapper implements Serializable {
	@SuppressWarnings("rawtypes")
	private List list;

	@SuppressWarnings("rawtypes")
	public DataWrapper(List list) {
		this.list = list;
	}
	
	@SuppressWarnings("rawtypes")
	public List getList() {
		return list;
	}
}
