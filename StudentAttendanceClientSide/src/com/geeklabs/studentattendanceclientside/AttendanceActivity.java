package com.geeklabs.studentattendanceclientside;

import java.util.List;

import android.annotation.SuppressLint;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.view.MenuItem;
import com.geeklabs.studentattendanceclientside.adapter.StudentCustomAdapter;
import com.geeklabs.studentattendanceclientside.domain.Attendance;
import com.geeklabs.studentattendanceclientside.util.DataWrapper;
import com.geeklabs.studentattendanceclientside.util.SIgnOutUtil;

public class AttendanceActivity extends ListActivity {
	private Intent intent;
	private StudentCustomAdapter studentCustomAdapter;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.student_attendance);
		intent = getIntent();
		List<Attendance> attendanceList = getAttendanceList();
		studentCustomAdapter = new StudentCustomAdapter(getApplicationContext());
		setAttendanceListToAdapter(attendanceList);
		//set studentName to studentNameTextView
		String studentName = intent.getStringExtra("name");
		TextView studentNameTextView = (TextView) findViewById(R.id.student_name);
		studentNameTextView.setText(studentName);
	}

	private void setAttendanceListToAdapter(List<Attendance> attendanceList) {
		if (studentCustomAdapter != null && !attendanceList.isEmpty()) {
			studentCustomAdapter.setAttendances(attendanceList);
			setListAdapter(studentCustomAdapter);
		}else {
			Toast.makeText(getApplicationContext(), "No attendanece for you", Toast.LENGTH_SHORT).show();
		}
	}

	@SuppressWarnings("unchecked")
	private List<Attendance> getAttendanceList() {
		DataWrapper dataWrapper = (DataWrapper) intent.getSerializableExtra("com.geeklabs.studentattendanceclientside.attendancelist");
		return dataWrapper.getList();
	}
	
	@SuppressLint("NewApi")
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add("SignOut").setTitle("Signout").setIcon(R.drawable.sign_out).setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(android.view.MenuItem item) {
		if (item.getTitle().equals("Signout")) {
			//create progress Dialog
			final ProgressDialog progressDialog = new ProgressDialog(this);
			progressDialog.setMessage("signOut is in progress...");
			Log.i("signOut process", "saving attendance is in progress");
			progressDialog.setIndeterminate(true);
			progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progressDialog.show();
			//signOut the user
			SIgnOutUtil.signOut(progressDialog, getApplicationContext());
			return true;
	}
		return super.onOptionsItemSelected(item);
	}
}
