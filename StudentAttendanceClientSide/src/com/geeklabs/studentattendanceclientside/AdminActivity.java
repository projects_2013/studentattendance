package com.geeklabs.studentattendanceclientside;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.actionbarsherlock.view.MenuItem;
import com.geeklabs.studentattendanceclientside.communication.task.post.SaveFacultyTask;
import com.geeklabs.studentattendanceclientside.communication.task.post.SaveStudentTask;
import com.geeklabs.studentattendanceclientside.domain.Faculty;
import com.geeklabs.studentattendanceclientside.domain.Student;
import com.geeklabs.studentattendanceclientside.util.SIgnOutUtil;

public class AdminActivity extends Activity {

	private Spinner userTypeSpinner, branchSpinner, yearSpinner;
	private String name;
	private EditText nameEditText, mailEditText, parentMailEditText, studentMobileEditText, parentMobileEditText;
	private TextView selectBranchTextView, selectYearTextView;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout);

		addItemsOnUserSpinner();
		addItemsOnBranchSpinner();
		addItemsOnYearSpinner();
		nameEditText = (EditText) findViewById(R.id.layout_username_edit_text);
		mailEditText = (EditText) findViewById(R.id.layout_maili_edittextd);
		parentMailEditText = (EditText) findViewById(R.id.layout_parent_mailid_edit_text);
		selectBranchTextView = (TextView) findViewById(R.id.layout_branchtextview);
		selectYearTextView = (TextView) findViewById(R.id.layout_year_text);
		branchSpinner = (Spinner) findViewById(R.id.layout_branchsprinner);
		yearSpinner = (Spinner) findViewById(R.id.layout_year_spinner);
		studentMobileEditText = (EditText) findViewById(R.id.layout_student_mobile_number_edittext);
		parentMobileEditText = (EditText) findViewById(R.id.layout_parent_mobile_edittext);
	}

	private void addItemsOnUserSpinner() {

		userTypeSpinner = (Spinner) findViewById(R.id.layout_selectusertype_spinner);
		List<String> list = new ArrayList<String>();
		list.add("Faculty");
		list.add("Student");
		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, list);
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		userTypeSpinner.setAdapter(dataAdapter);
		userTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				name = userTypeSpinner.getSelectedItem().toString();

				if (name.equals("Faculty")) {
					branchSpinner.setVisibility(View.INVISIBLE);
					yearSpinner.setVisibility(View.INVISIBLE);
					selectBranchTextView.setVisibility(View.INVISIBLE);
					selectYearTextView.setVisibility(View.INVISIBLE);
					parentMailEditText.setVisibility(View.INVISIBLE);
					parentMobileEditText.setVisibility(View.INVISIBLE);
					studentMobileEditText.setVisibility(View.INVISIBLE);
				} else {
					branchSpinner.setVisibility(View.VISIBLE);
					yearSpinner.setVisibility(View.VISIBLE);
					selectBranchTextView.setVisibility(View.VISIBLE);
					selectYearTextView.setVisibility(View.VISIBLE);
					parentMailEditText.setVisibility(View.VISIBLE);
					parentMobileEditText.setVisibility(View.VISIBLE);
					studentMobileEditText.setVisibility(View.VISIBLE);
				}
			//	Toast.makeText(getApplicationContext(), name, Toast.LENGTH_LONG).show();
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {

			}
		});
	}

	private void addItemsOnBranchSpinner() {
		branchSpinner = (Spinner) findViewById(R.id.layout_branchsprinner);
		List<String> list = new ArrayList<String>();
		list.add("CSE");
		list.add("IT");
		list.add("ECE");
		list.add("EEE");
		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, list);
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		branchSpinner.setAdapter(dataAdapter);
		branchSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				name = branchSpinner.getSelectedItem().toString();
				//Toast.makeText(getApplicationContext(), name, Toast.LENGTH_LONG).show();
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {

			}
		});
	}

	private void addItemsOnYearSpinner() {
		yearSpinner = (Spinner) findViewById(R.id.layout_year_spinner);
		List<String> list = new ArrayList<String>();
		list.add("First");
		list.add("Second");
		list.add("Third");
		list.add("Fourth");
		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, list);
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		yearSpinner.setAdapter(dataAdapter);
		yearSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				name = yearSpinner.getSelectedItem().toString();
			//	Toast.makeText(getApplicationContext(), name, Toast.LENGTH_LONG).show();
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {

			}
		});
	}

	@SuppressLint("NewApi")
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add("save").setIcon(R.drawable.save).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		menu.add("signout").setIcon(R.drawable.sign_out).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(android.view.MenuItem item) {
		if (item.getTitle().equals("save")) {

			/*findViewById(R.id.done_button2).setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {}
			});*/

			name = userTypeSpinner.getSelectedItem().toString();
			if (name.equals("Faculty")) {
				//create progressDialog
				final ProgressDialog progressDialog = new ProgressDialog(AdminActivity.this);
				progressDialog.setMessage("saving Faculty :) ");
				progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
				progressDialog.setIndeterminate(true);
				progressDialog.show();
				
				//create faculty
				Faculty faculty = new Faculty();
				String facultyName = nameEditText.getText().toString();
				String facultyMail = mailEditText.getText().toString();
				faculty.setName(facultyName);
				faculty.setEmail(facultyMail);
				//call SaveFacultyTask to save Faculty data
				SaveFacultyTask saveFacultyTask = new SaveFacultyTask(progressDialog, AdminActivity.this, faculty);
				saveFacultyTask.execute();
				
				// setting fields to null
				nameEditText.setText("");
				mailEditText.setText("@gmail.com");
			}else if (name.equals("Student")) {
				//create progressDialog
				final ProgressDialog progressDialog = new ProgressDialog(AdminActivity.this);
				progressDialog.setMessage("saving Student :) ");
				progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
				progressDialog.setIndeterminate(true);
				progressDialog.show();
				
				//create student
				Student student = new Student();
				String branch = branchSpinner.getSelectedItem().toString();
				String studentName = nameEditText.getText().toString();
				student.setBranch(branch);
				student.setName(studentName);
				String parentEmail = parentMailEditText.getText().toString();
				student.setParentEmail(parentEmail);
				String studentEmail = mailEditText.getText().toString();
				student.setStudentEmail(studentEmail);
				String year = yearSpinner.getSelectedItem().toString();
				student.setYear(year);
				String mobileNum = studentMobileEditText.getText().toString();
				student.setStudentMobileNumber(mobileNum);
				String parentMobNum = parentMobileEditText.getText().toString();
				student.setParentMobileNumber(parentMobNum);
				
				//call SaveFacultyTask to save Faculty data
				SaveStudentTask saveStudentTask = new SaveStudentTask(progressDialog, AdminActivity.this, student);
				saveStudentTask.execute();
			
				// setting fields to null
				nameEditText.setText("");
				studentMobileEditText.setText("");
				parentMobileEditText.setText("");
				mailEditText.setText("@gmail.com");
				parentMailEditText.setText("@gmail.com");
			}
		
		
			
		}else if(item.getTitle().equals("signout")){
			//create progress Dialog
			final ProgressDialog progressDialog = new ProgressDialog(AdminActivity.this);
			progressDialog.setMessage("signOut is in progress...");
			Log.i("signOut process", "saving attendance is in progress");
			progressDialog.setIndeterminate(true);
			progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progressDialog.show();
			
			SIgnOutUtil.signOut(progressDialog, getApplicationContext());
			return true;
		}else {
			return false;
		}
		return false;
	}
}
