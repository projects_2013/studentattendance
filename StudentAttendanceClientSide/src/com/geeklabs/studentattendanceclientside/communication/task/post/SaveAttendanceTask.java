package com.geeklabs.studentattendanceclientside.communication.task.post;

import java.io.IOException;
import java.util.List;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONException;

import android.app.Activity;
import android.app.ProgressDialog;
import android.util.Log;
import android.widget.Toast;

import com.geeklabs.studentattendanceclientside.communication.AbstractHttpPostTask;
import com.geeklabs.studentattendanceclientside.domain.Attendance;
import com.geeklabs.studentattendanceclientside.util.RequestUrl;
import com.geeklabs.studentattendanceclientside.util.ResponseStatus;

public class SaveAttendanceTask extends AbstractHttpPostTask {

	private Activity context;
	private List<Attendance> attendanceList;

	public SaveAttendanceTask(ProgressDialog progressDialog, Activity context,
			List<Attendance> attendanceList) {
		super(progressDialog, context);
		this.progressDialog = progressDialog;
		this.context = context;
		this.attendanceList = attendanceList;
	}

	@Override
	protected String getRequestJSON() {
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.writeValueAsString(attendanceList);
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	protected String getRequestUrl() {
		return RequestUrl.SAVE_ATTENDANCE;
	}

	@Override
	protected void showMessageOnUI(final String message) {
		context.runOnUiThread(new Runnable() {

			@Override
			public void run() {
				Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
				Log.i("SAS Request Processing", message);
			}
		});
	}

	@Override
	protected void handleResponse(String jsonResponse) throws JSONException {
		ObjectMapper mapper = new ObjectMapper();
		try {
			ResponseStatus status = mapper.readValue(jsonResponse.toString(), ResponseStatus.class);
			if ("success".equals(status.getStatus())) {
				showMessageOnUI("Saved your attendance");
			} else {
				showMessageOnUI("Try again");
			}
		} catch (Exception exception) {
			exception.printStackTrace();
		} finally {
			cancelDialog();
		}
	}
}