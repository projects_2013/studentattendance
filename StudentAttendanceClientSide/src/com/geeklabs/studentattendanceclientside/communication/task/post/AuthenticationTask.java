package com.geeklabs.studentattendanceclientside.communication.task.post;

import java.io.IOException;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONException;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.widget.Toast;

import com.geeklabs.studentattendanceclientside.AdminHomeActivity;
import com.geeklabs.studentattendanceclientside.FacultyActivity;
import com.geeklabs.studentattendanceclientside.StudentOptionsActivity;
import com.geeklabs.studentattendanceclientside.communication.AbstractHttpPostTask;
import com.geeklabs.studentattendanceclientside.domain.LoginAs;
import com.geeklabs.studentattendanceclientside.preferences.AuthPreferences;
import com.geeklabs.studentattendanceclientside.util.RequestUrl;
import com.geeklabs.studentattendanceclientside.util.ResponseStatus;

public class AuthenticationTask extends AbstractHttpPostTask {

	private LoginAs loginAs;
	private Activity context;
	private Intent intent;
	private AuthPreferences authPreferences;
	private String authToken;
	private String loginType;
	private Long id;
	private Long studentId;
	private String branch;
	private String year;

	public AuthenticationTask(ProgressDialog progressDialog, Activity context, LoginAs loginAs) {
		super(progressDialog, context);
		this.context = context;
		this.loginAs = loginAs;
		authPreferences = new AuthPreferences(context);
	}

	@Override
	protected String getRequestJSON() {
		ObjectMapper mapper = new ObjectMapper();
		String loginReq = null;
		try {
			loginReq = mapper.writeValueAsString(loginAs);
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return loginReq;
	}

	@Override
	protected String getRequestUrl() {
		return RequestUrl.VALIDATE_TOKEN;
	}

	@Override
	protected void showMessageOnUI(final String message) {
		Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
	}

	@SuppressLint("NewApi")
	@Override
	protected void handleResponse(String jsonResponse) throws JSONException {
		if (!jsonResponse.isEmpty()) {
			ObjectMapper mapper = new ObjectMapper();
			ResponseStatus responseStatus = null;
			try {
				responseStatus = mapper.readValue(jsonResponse, ResponseStatus.class);
				String status = responseStatus.getStatus();
				String name = null;
				if (responseStatus.getId() != null && responseStatus.getName() != null) {
					id = responseStatus.getId();
					name = responseStatus.getName();
					studentId = responseStatus.getStudentId();
					branch = responseStatus.getBranch();
					year = responseStatus.getYear();
				}
				authToken = loginAs.getAuthToken();
				loginType = loginAs.getLoginType();

				if (status.equals("Admin")) {
					
					//set properties
					setProperties();
					
					intent = new Intent(context, AdminHomeActivity.class);
					intent.putExtra("name", name);
					intent.putExtra("id", id);
					context.startActivity(intent);
				}  else if (status.equals("Student")) {
					
					//set properties
					setProperties();
					
					intent = new Intent(context, StudentOptionsActivity.class);
					intent.putExtra("name", name);
					intent.putExtra("id", id);
					intent.putExtra("branch", branch);
					intent.putExtra("year", year);
					context.startActivity(intent);
					
				}  else if (status.equals("Faculty")) {
					
					//set properties
					setProperties();
					
					intent = new Intent(context, FacultyActivity.class);
					intent.putExtra("name", name);
					intent.putExtra("id", id);
					context.startActivity(intent);
				}else if (status.equals("SigninStatus")) {
					AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
					// set title
					alertDialogBuilder.setTitle("Login Issue");
					// set dialog message
					alertDialogBuilder.setMessage("With this credentials some where logged else please contact administrator.");
					alertDialogBuilder.setCancelable(false).setNegativeButton(
							"Ok", new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
								}
							});
					// create alert dialog
					AlertDialog alertDialog = alertDialogBuilder.create();

					// show it
					alertDialog.show();
				} else if(status.equals("error")) {
					showMessageOnUI("wrong userid or password ");
				} else {
					showMessageOnUI("you don't have a login access for this login type");
				}
			} catch (JsonParseException e) {
				e.printStackTrace();
			} catch (JsonMappingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				cancelDialog();
			}
		}else {
			showMessageOnUI("problem occured while handling req..");
		}
	}

	private void setProperties() {
		// Set user signIn status
		authPreferences.setSignInStatus(true);
		// Set user accessToken in status
		authPreferences.setAccessToken(authToken);
		// Set user loginType in status
		authPreferences.setLoginType(loginType);
		// Set user userId in status
		authPreferences.setUserId(id);

	}
}
