package com.geeklabs.studentattendanceclientside.communication.task.post;

import java.io.IOException;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONException;

import android.app.ProgressDialog;
import android.content.Context;
import android.widget.Toast;

import com.geeklabs.studentattendanceclientside.communication.AbstractHttpPostTask;
import com.geeklabs.studentattendanceclientside.domain.Student;
import com.geeklabs.studentattendanceclientside.util.RequestUrl;
import com.geeklabs.studentattendanceclientside.util.ResponseStatus;

public class SaveStudentTask extends AbstractHttpPostTask {

	private Context context;
	private Student student;

	public SaveStudentTask(ProgressDialog progressDialog, Context context, Student student) {
		super(progressDialog, context);
		this.context = context;
		this.student = student;
	}

	@Override
	protected String getRequestJSON() {
		ObjectMapper mapper = new ObjectMapper();
		String studentDetails = null;
		try {
			studentDetails = mapper.writeValueAsString(student);
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return studentDetails;
	}

	@Override
	protected String getRequestUrl() {
		return RequestUrl.SAVE_STUDENT;
	}

	@Override
	protected void showMessageOnUI(String message) {
		Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
	}

	@Override
	protected void handleResponse(String jsonResponse) throws JSONException {
		if (!jsonResponse.isEmpty()) {
			ObjectMapper mapper = new ObjectMapper();
			try {
				ResponseStatus responseStatus = mapper.readValue(
						jsonResponse.toString(), ResponseStatus.class);
				String status = responseStatus.getStatus();
				if (status.contains("success")) {
					showMessageOnUI("Student saved :)");
					/*Intent intent = new Intent(context, AdminActivity.class);
					context.startActivity(intent);*/
				} else if (isNetworkAvailable) {
					showMessageOnUI("This Student Already");
				} else {
					showMessageOnUI("Student not saved tryAgain:)");
					/*Intent intent = new Intent(context, AdminActivity.class);
					context.startActivity(intent);*/
				}
			} catch (JsonParseException e) {
				e.printStackTrace();
			} catch (JsonMappingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				cancelDialog();
			}
		}
	}

}
