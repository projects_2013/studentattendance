package com.geeklabs.studentattendanceclientside.communication.task.post;

import java.io.IOException;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONException;

import android.app.ProgressDialog;
import android.content.Context;
import android.widget.Toast;

import com.geeklabs.studentattendanceclientside.communication.AbstractHttpPostTask;
import com.geeklabs.studentattendanceclientside.domain.Faculty;
import com.geeklabs.studentattendanceclientside.util.RequestUrl;
import com.geeklabs.studentattendanceclientside.util.ResponseStatus;

public class SaveFacultyTask extends AbstractHttpPostTask {
	
	private Context context;
	private Faculty faculty;
	public SaveFacultyTask(ProgressDialog progressDialog, Context context, Faculty faculty) {
		super(progressDialog, context);
		this.context = context;
		this.faculty = faculty;
	}

	@Override
	protected String getRequestJSON() {
		ObjectMapper mapper = new ObjectMapper();
		String facultyDetails = null;
		try {
			facultyDetails = mapper.writeValueAsString(faculty);
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return facultyDetails;
	}

	@Override
	protected String getRequestUrl() {
		return RequestUrl.SAVE_FACULTY;
	}

	@Override
	protected void showMessageOnUI(String message) {
		Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
	}

	@Override
	protected void handleResponse(String jsonResponse) throws JSONException {
		if (!jsonResponse.isEmpty()) {
			ObjectMapper mapper = new ObjectMapper();
			try {
				ResponseStatus responseStatus = mapper.readValue(jsonResponse.toString(), ResponseStatus.class);
				String status = responseStatus.getStatus();
				if (status.contains("success")) {
					showMessageOnUI("Faculty saved :)");
					//Intent intent = new Intent(context, AdminActivity.class);
					//context.startActivity();
					
				} else if (status.contains("Faculty Already Existed")) {
					showMessageOnUI("This Faculty Already Exist");
				} else {
					showMessageOnUI("Faculty not saved tryAgain:)");
					//Intent intent = new Intent(context, AdminActivity.class);
					//context.startActivity(intent);
				}
			} catch (JsonParseException e) {
				e.printStackTrace();
			} catch (JsonMappingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}finally{
				cancelDialog();
			}
		}
	}
}
