package com.geeklabs.studentattendanceclientside.communication.task.get;

import java.io.IOException;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONException;

import android.accounts.AccountManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.geeklabs.studentattendanceclientside.HomeActivity;
import com.geeklabs.studentattendanceclientside.communication.AbstractHttpGetTask;
import com.geeklabs.studentattendanceclientside.preferences.AuthPreferences;
import com.geeklabs.studentattendanceclientside.util.RequestUrl;
import com.geeklabs.studentattendanceclientside.util.ResponseStatus;

public class SignOutTask extends AbstractHttpGetTask {

	private Context context;
	private AuthPreferences authPreferences;

	public SignOutTask(ProgressDialog progressDialog, Context context) {
		super(progressDialog, context);
		this.context = context;
		authPreferences = new AuthPreferences(context);
	}

	@Override
	protected String getRequestUrl() {
		String id = String.valueOf(authPreferences.getUserId());
		String loginType = authPreferences.getLoginType();
		return RequestUrl.SIGNOUT.replace("{id}", id).replace("{loginType}", loginType);
	}

	@Override
	protected void showMessageOnUI(final String message) {
		Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
	}

	@Override
	protected void handleResponse(String jsonResponse) throws JSONException {
		if (!jsonResponse.isEmpty()) {
			ObjectMapper mapper = new ObjectMapper();
			try {
				ResponseStatus responseStatus = mapper.readValue(jsonResponse, ResponseStatus.class);
				String status = responseStatus.getStatus();
				if (status.equals("Signout")) {
					// Request signout to Google server
					signOut();
					// Set user signin status 
					authPreferences.setSignInStatus(false);
					// Set user LoginType status
					authPreferences.setLoginType(null);
					// Set user AccessToken status
					authPreferences.setAccessToken(null);
					// Set user UserId status
					authPreferences.setUserId(0);
					
					Intent i = new Intent(context, HomeActivity.class);
					i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					context.startActivity(i);
					showMessageOnUI(" Your successfully logged out");
					cancelDialog();
				} else {
					showMessageOnUI("Unable to reach server, try after sometime");
					cancelDialog();
				}
			} catch (JsonParseException e) {
				e.printStackTrace();
			} catch (JsonMappingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}

	private void signOut() {
		AccountManager accountManager = AccountManager.get(context);
		accountManager.invalidateAuthToken("com.google", authPreferences.getAccessToken());
		authPreferences.setAccessToken(null);
	}
}
