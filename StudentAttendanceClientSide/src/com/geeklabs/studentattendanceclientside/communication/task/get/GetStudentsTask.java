package com.geeklabs.studentattendanceclientside.communication.task.get;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.json.JSONException;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.geeklabs.studentattendanceclientside.StudentActivity;
import com.geeklabs.studentattendanceclientside.StudentListActivity;
import com.geeklabs.studentattendanceclientside.communication.AbstractHttpGetTask;
import com.geeklabs.studentattendanceclientside.domain.Student;
import com.geeklabs.studentattendanceclientside.domain.UtilDomain;
import com.geeklabs.studentattendanceclientside.preferences.AuthPreferences;
import com.geeklabs.studentattendanceclientside.util.DataWrapper;
import com.geeklabs.studentattendanceclientside.util.RequestUrl;

public class GetStudentsTask extends AbstractHttpGetTask {
	private Activity contextActivity;
	private UtilDomain utilDomain;
	private String branch;
	private String year;
	private AuthPreferences authPreferences;

	public GetStudentsTask(Activity context, ProgressDialog progressDialog, UtilDomain utilDomain) {
		super(progressDialog, context);
		this.contextActivity = context;
		this.utilDomain = utilDomain;
		authPreferences = new AuthPreferences(context);
	}

	@Override
	protected String getRequestUrl() {
		 branch = utilDomain.getBranch();
		 year  = utilDomain.getYear();
		return RequestUrl.GET_STUDENTS.replace("{branch}",branch).replace("{year}", year);
	}

	@Override
	protected void showMessageOnUI(final String message) {
		contextActivity.runOnUiThread(new Runnable() {
			public void run() {
				Toast.makeText(contextActivity, message, Toast.LENGTH_SHORT)
						.show();
				Log.i("Loading ...", message);
			}
		});

	}

	@SuppressLint("NewApi")
	@Override
	protected void handleResponse(String jsonResponse) throws JSONException {
		List<Student> students = new ArrayList<Student>();
		if (!jsonResponse.isEmpty()) {
			ObjectMapper mapper = new ObjectMapper();
			try {
				students = mapper.readValue(jsonResponse,
						new TypeReference<List<Student>>() {
						});
				if (!students.isEmpty() && authPreferences.getLoginType().equals("Faculty")) {
					Intent intent = new Intent(contextActivity, StudentActivity.class);
					intent.putExtra("com.geeklabs.studentattendanceclientside.studentlist", new DataWrapper(students));
					intent.putExtra("com.geeklabs.studentattendanceclientside.utildata", utilDomain);
					contextActivity.startActivity(intent);
				}else if(authPreferences.getLoginType().equals("Admin")) {
					Intent intent = new Intent(contextActivity, StudentListActivity.class);
					intent.putExtra("com.geeklabs.studentattendanceclientside.studentlist", new DataWrapper(students));
					intent.putExtra("com.geeklabs.studentattendanceclientside.utildata", utilDomain);
					contextActivity.startActivity(intent);
				}
			} catch (JsonParseException e) {
				e.printStackTrace();
				showMessageOnUI("problem occured while handling request");
			} catch (JsonMappingException e) {
				e.printStackTrace();
				showMessageOnUI("problem occured while handling request");
			} catch (IOException e) {
				e.printStackTrace();
				showMessageOnUI("problem occured while handling request");
			}finally {
				 cancelDialog();
			}
		}
	}
}
