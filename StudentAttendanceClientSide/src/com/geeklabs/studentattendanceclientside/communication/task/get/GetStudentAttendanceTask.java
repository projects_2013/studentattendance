package com.geeklabs.studentattendanceclientside.communication.task.get;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.json.JSONException;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.geeklabs.studentattendanceclientside.AttendanceActivity;
import com.geeklabs.studentattendanceclientside.communication.AbstractHttpGetTask;
import com.geeklabs.studentattendanceclientside.domain.Attendance;
import com.geeklabs.studentattendanceclientside.preferences.AuthPreferences;
import com.geeklabs.studentattendanceclientside.util.DataWrapper;
import com.geeklabs.studentattendanceclientside.util.RequestUrl;

public class GetStudentAttendanceTask extends AbstractHttpGetTask {

	private Context context;
	private String studentName;
	private Intent intent;
	private AuthPreferences authPreferences;
	private long studentId;

	public GetStudentAttendanceTask(ProgressDialog progressDialog, Context context, String studentName) {
		super(progressDialog, context);
		this.studentName = studentName;
		this.context = context;
		authPreferences = new AuthPreferences(context);
	}

	public GetStudentAttendanceTask(ProgressDialog progressDialog, Context context, long studentId, String studentName) {
		super(progressDialog, context);
		this.context = context;
		this.studentId = studentId;
		this.studentName = studentName;
	}
	
	@Override
	protected String getRequestUrl() {
		
		if (studentId > 1) {
			return RequestUrl.SPECIFIC_STUDENT_ATTENDANCE.replace("{id}", String.valueOf(studentId));
		} else {
			return RequestUrl.SPECIFIC_STUDENT_ATTENDANCE.replace("{id}", String.valueOf(authPreferences.getUserId()));
		}
	}

	@Override
	protected void showMessageOnUI(final String message) {
		Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
	}

	@Override
	protected void handleResponse(String jsonResponse) throws JSONException {
		List<Attendance> attendances = new ArrayList<Attendance>();
		try {
			ObjectMapper mapper = new ObjectMapper();
			attendances = mapper.readValue(jsonResponse,
					new TypeReference<List<Attendance>>() {
					});
			if (!attendances.isEmpty()) {
				intent = new Intent(context, AttendanceActivity.class);
				intent.putExtra("name", studentName);
				intent.putExtra("com.geeklabs.studentattendanceclientside.attendancelist", new DataWrapper(attendances));
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				/*intent.addFlags(Intent.FLAG_ACTIVITY_MULTIPLE_TASK);*/
				context.startActivity(intent);
			} else {
				/*intent = new Intent(context, StudentInfo.class);
				context.startActivity(intent);*/
				showMessageOnUI("no attendance for you");
			}
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			cancelDialog();
		}
	}
}
