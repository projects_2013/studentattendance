package com.geeklabs.studentattendanceclientside.communication.task.get;

import org.json.JSONException;

import android.app.ProgressDialog;
import android.content.Context;

import com.geeklabs.studentattendanceclientside.communication.AbstractHttpGetTask;
import com.geeklabs.studentattendanceclientside.util.RequestUrl;

public class GetFacultyTask extends AbstractHttpGetTask {

	public GetFacultyTask(ProgressDialog progressDialog, Context context,String facultyName) {
		super(progressDialog, context);
	}

	@Override
	protected String getRequestUrl() {
		return RequestUrl.GET_FACULTY;
	}

	@Override
	protected void showMessageOnUI(String string) {
		
	}

	@Override
	protected void handleResponse(String jsonResponse) throws JSONException {
		
	}

}
