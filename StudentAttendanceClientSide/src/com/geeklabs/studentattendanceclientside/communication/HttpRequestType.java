package com.geeklabs.studentattendanceclientside.communication;

public enum HttpRequestType {

	GET, POST, PUT, PATCH, DELET
}
