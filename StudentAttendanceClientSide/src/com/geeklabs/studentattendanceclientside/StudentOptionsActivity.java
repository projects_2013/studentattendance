package com.geeklabs.studentattendanceclientside;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.geeklabs.studentattendanceclientside.communication.task.get.GetStudentAttendanceTask;
import com.geeklabs.studentattendanceclientside.util.SIgnOutUtil;

public class StudentOptionsActivity extends Activity {

	private Button assignmentsButton, attendanceButton, feeButton,
			notificationButton, resultsButton;
	private Intent intent;
	private String studentName, branch, year;
	private long studentId;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_student_options);
		
		intent = getIntent();
		studentName = intent.getStringExtra("name");
		branch = intent.getStringExtra("branch");
		year = intent.getStringExtra("year");
		studentId = intent.getLongExtra("id", 1);

		attendanceButton = (Button) findViewById(R.id.student_option_activity_attendance_button);

		onClickOnAttendanceButton();
	}

	private void onClickOnAttendanceButton() {
		attendanceButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// get student attendance
				ProgressDialog progressDialog = callProgressDialog();
				GetStudentAttendanceTask getStudentAttendanceTask = new GetStudentAttendanceTask(progressDialog, getApplicationContext(), studentId, studentName);
				getStudentAttendanceTask.execute();
			}

		});
	}

	
	private ProgressDialog callProgressDialog() {
		// create progressDialog
		final ProgressDialog progressDialog = new ProgressDialog(StudentOptionsActivity.this);
		progressDialog.setMessage("please wait a min...");
		progressDialog.setCancelable(false);
		return progressDialog;

	}
	
@SuppressLint("NewApi")
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.faculty, menu);
		menu.add("Sign Out").setTitle("Sign out").setIcon(R.drawable.sign_out).setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(android.view.MenuItem item) {
		if (item.getTitle().equals("Sign out")) {
			//create progress Dialog
			final ProgressDialog progressDialog = new ProgressDialog(this);
			progressDialog.setMessage("signout is in progress...");
			Log.i("signout process", "signout is in progress");
			progressDialog.setIndeterminate(true);
			progressDialog.setCancelable(false);
			progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progressDialog.show();
			//signOut the user
			SIgnOutUtil.signOut(progressDialog, getApplicationContext());
			return true;
	}
		return false;
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		moveTaskToBack(true);
	}
}
